# Building and Training Neural Networks

In the previous chapter we learned about how neural networks work in principle. We also saw that we can apply a neural network to a chemical problem: The folding energy of simple toy proteins can be reliably predicted with a simple multi-layer perceptron (MLP), using only the sequence as input. In this chapter we will discuss the details of how to train and design neural networks in practice. To do this, we will first talk about the big elephant in the room, when it comes to NNs: overfitting.

## Overfitting (and how to prevent it)

When we discussed linear regression, we saw that using a large number of polynomial features leads to regression models that fit the data perfectly, but have very large errors on an external validation set. We called these models *overfitted*. Another way to put this is to say that the models do not *generalize* well to data that they are not fitted to. In linear regression and classical statistics, overfitting is commonly associated with the number of parameters in the model, and we usually aim for keeping the number of parameters significantly smaller than the number of training datapoints. As you probably noticed, however, NNs have *a lot* of parameters. For example, the NN used in the previous chapter has 6592 trainable weights and biases, while our training set only had 3000 examples. In comparison, the corresponding linear model only has 36 parameters (i.e. the number of features in the representation). So what is going on? Is overfitting not a problem for NNs? And if yes, what can we do about it? 

### Regularization

The answers to these questions relate to the concept of *regularization*. Indeed, regularization is probably the most important concept in ML. The key advantage of ML models is their large flexibility. This means that they can basically fit any dataset (see the universal approximation theorems mentioned in the previous chapter). In fact, there are in principle infinitely many different NNs that can fit a given dataset: 

```{figure} notebooks/reg_ensemble1.png
:alt: "an ensemble of functions fitting three datapoints"
:width: 400px
:align: center

An ensemble of 50 functions which perfectly fit three datapoints but differ from each other strongly otherwise.
```

The goal of regularization is to bias the training procedure in such a way that the *best* function is selected out of these infinite possibilities. Of course, this raises another question: What is the *best* function? There is no unique answer here, but in general, regularization methods tend to work like "Occam's Razor": If you have many models that fit the data, you should choose the one that is as simple and as smooth as possible. There are some mathematical measures of simplicity and smoothness that can be used to this end. In practice, it is very common to instead consider the generalizability of the models instead, using an external validation set.

Regularizers come in many shapes and forms and can even just be implicit features of a certain model or training procedure. Basically, in ML language anything that improves generalization and reduces overfitting is called a regularizer. In this sense, the selection of polynomial basis functions can also be considered a form of regularization: By reducing the number of polynomial basis functions, we ensure that the fit function is smoother and generalizes more reliably. The point is that this is only one possible form of regularization. In other words, the solution to the NN overfitting dilemma is that we are free to use ultra-flexible models with millions of parameters, as long as we make sure that they are regularized.

### Double Descent

An interesting phenomenon that captures the relation between conventional curve fitting and ML methods is the so-called *double descent*. This is often illustrated by a plot of the capacity of a model (roughly corresponding to the number of trainable parameters) against the training and validation loss: 

```{figure} notebooks/double_descent.png
:alt: "illustration of double descent"
:width: 400px
:align: center

Illustration of the double descent phenomenon. Increasing the capacity of a model initially leads to a decrease in training and validation loss (first descent). The validation loss then begins to rise again due to overfitting. If the capacity of the models is increased even further, the validation loss begins to decrease gain (second descent). The validation loss maximum separates underparameterized and overparameterized models.
```

We saw the first part of this plot already, when we discussed linear regression. By increasing the capacity of a model both training and validation errors are initially decreased. However, at some point the validation error starts to saturate and subsequently increases again, due to overfitting. The beauty of double descent is that as we further increase the capacity of the model even further, the validation error begins to go down again. 

We can characterize the models on both sides of the maximum validation loss as *underparameterized* and *overparameterized*, respectively. Underparameterized models have significantly fewer degrees of freedom than datapoints. They are therefore unable to perfectly fit the training data. On the plus side, they can provide robust predictions, with approximately equal loss for the training and validation. As we approach the maximum loss, this robustness is lost. The model now has just enough capacity to fit the training data almost perfectly, but no longer generalizes to the validation set. If we further increase the capacity, the models become overparameterized, as in the ensemble of functions shown above. Now we not only have enough freedom to fit the training data with high accuracy, but we can also obtain models that generalize well by selecting the most regular function that fits the data.

While the double descent picture seems to imply that it is always best to use models that are as large as possible, there are certain technical aspects to keep in mind in practice. In particular, very deep models are harder to train (instabilities, vanishing gradients) and more demanding in terms of computational resources. Furthermore, the generalization capabilities of even the deepest model is ultimately limited by the training data. It thus makes no sense to fit an enormous neural network on a very small dataset. 

## Practical Tips for Building and Training Neural Networks

With this background in mind, let's return to the model we fitted in the previous chapter. As you may remember, we had to make a lot of choices before fitting the model, such as defining a representation, optimizer, network architecture, etc. We can now consider these choices in more detail, through the lense of regularization.

## Training Set Size

Theoretical discussion about universal approximation and double descent typically assume infinitely large models and infinite training data. Both of these things are far from the truth in practice. So, before we get into the details of model building, it is important to emphasize that the number one thing that will almost always improve your model is more data. This can be illustrated by a *learning curve*, where the test set error of different models is plotted against the size of the training set:

```{figure} notebooks/learning_curve_test_vs_ntrain.png
:alt: "test error vs training set size"
:width: 400px
:align: center

Learning curve of test set error versus training set size. Note the linear relationship between the two on a log-log scale. The plot is for folding energy prediction on the TinyFold data set, using NNs trained with 500, 1000, 1500 and 3000 samples, respectively (2 hidden layers, 64 neurons per layer).
```

These curves are typically plotted on a log-log scale, where they tend to display a linear relationship. This means that increasing the training data will systematically decrease the error on an external test set. In this sense, the term machine *learning* is very appropriate. ML models improve by seeing more data, which is not true for classical underparameterized models.

Confusingly, the term *learning curve* is also commonly used for plots of training and/or validation loss against the number training epochs:

```{figure} notebooks/learning_curve_val_n_train.png
:alt: "learning curves for different training set size"
:width: 400px
:align: center

Learning curves of validation loss versus training epochs for models with different training set sizes (2 hidden layers, 64 neurons per layer).
```

On one hand, this ambiguity is unfortunate. On the other hand both types of plots illustrate the way ML models learn from data, so the term fits. In the latter plot, we see how the models improve by repeatedly scanning through their training sets. Initially, all models improve, but the overall achievable validation loss is ultimately limited by the size of the training set.

## Implicit Regularizers

As mentioned above, overparameterized models such as NNs must be regularized. However, we already fit a fairly good NN model in the previous chapter, without discussing regularization at all. This is because ML models benefit from a number of so-called *implicit regularizers*, which we discuss in the following.

### Gradient Descent and Early Stopping

As it turns out, optimizing the parameters with gradient descent is already a form of regularization. The basic idea behind this is that the parameters are modified in the direction of the largest improvement with every update. Since we start with a random network, the largest improvement is initially obtained with changes that improve the description of many training examples simultaneously. In this way, NNs prefer to learn generalizable aspects from the training set in early epochs, which is why validation and training errors are often very close in the first part of the learning curve. When we train for many epochs, all generalizable traits of the training set are learned and the only way to improve the model is to *memorize* features of specific training examples. This further decreases the training loss, but not the validation loss (*i.e.* it leads to overfitting).

```{figure} notebooks/learning_curve_tiny2.png
:alt: "train and validation learning curve of folding energy prediction"
:width: 400px
:align: center

Learning curve for folding energy prediction in the TinyFold dataset. The validation error saturates earlier than the training error (2 hidden layers, 64 neurons per layer).
```
To take advantage of this property of gradient descent, it is common to interrupt the training process when the validation loss stops decreasing. This is called *early stopping*. Here it is important to note that the validation loss can be somewhat noisy (especially if the validation set is small), so early stopping criteria should not be too strict. If training is stopped as soon as the validation loss increases for the first time, you will likely end up with a suboptimal model. Instead, one can look at a moving average of the validation loss or simply select the model with the lowest validation error after running a fixed number of epochs. In the following, we use the latter choice.

### Optimizers

Because gradient based optimization has a regularizing effect, it makes sense that the specific choice of the gradient descent algorithm also influences the generalization error of the model. In mini-batch stochastic gradient descent (SGD), these choices are the learning rate $\gamma$ and the batch size. However, conventional SGD is not commonly employed for training modern NNs, since different extensions of SGD have been found to lead to better results. The simplest of these (which we already used in the last chapter) is *momentum*. This essentially introduces some history-dependence to the optimizer. Specifically, the parameter update is performed as:

$$
\mathbf{W}_{T+1} = \mathbf{W}_{T} + \Delta \mathbf{W}_{T},
$$

with

$$
\Delta \mathbf{W}_{T} = - \gamma \frac{\partial \mathcal{L_{T}}}{\partial \mathbf{W}} + \mu \Delta \mathbf{W}_{T-1}
$$

Here, the first part of the update function is identical to the SGD algorithm we discussed previously. However, there is also a momentum contribution, which is simply the weight update $\Delta \mathbf{W}_{T-1}$ from the previous step, scaled by a facor $\mu<1$. This means that the update will continue moving in the same direction as the previous step, like a ball rolling down a hill. This reduces the commonly observed oscillatory behaviour of SGD. 

Another way to improve SGD is by using a more sophisticated version of the learning rate. In conventional SGD, the same rate is used for all weights, but in practice some weights change much more than others. Here, it can be useful to use a lower learning rate for weights that change a lot (i.e. because they have had large gradients in previous iterations). This is the idea used in the *RMSprop* or *AdaGrad* optimizers. Finally, the *Adam* optimizer combines the adaptive learning rates of RMSprop with momentum. It is a common default for a wide range of applications and we use it for all models in this chapter.

If you have worked with gradient based optimizers before, you may wonder about the issue of local and global minima. It is well known that gradient descent only allows finding nearby local minima from a given starting point. Meanwhile, global optimization of a non-convex function with thousands (or even millions) of parameters is a very challenging task. This is also true for NNs. If we initialize five otherwise identical NNs with different random parameter sets, we will usually end up with five different models:

```{figure} notebooks/learning_curve_val_models.png
:alt: "validation learning curves for different model initializations"
:width: 400px
:align: center

Learning curves for validation loss of differently initialized but otherwise identical neural networks (2 hidden layers, 64 neurons per layer).
```

As can be seen from the curves, the models follow somewhat different learning curves but all end up with similar validation errors. The differences in these curves do not simply stem from the stochastic nature of the optimizer, however. This can be seen by plotting the correlations between the weights in the final layer of each model:

```{figure} notebooks/model_parameter_correlations.png
:alt: "correlation between final layer weights for different models"
:width: 700px
:align: center

Correlations between final layer weights for differently initialized but otherwise identical neural networks (2 hidden layers, 64 neurons per layer).
```

Indeed, there is no correlation between the weights, indicating that all models converged to different local minima. This is a fortunate consequence of the high capacity of NNs: we do not need to find the global minimum, as there are in many different parameter sets that can fit our data accurately. That being said, there is always the danger of ending up in a 'bad' local minimum. In practice, this is typically avoided by fitting multiple models from different intializations. Later on we will discuss that these different parameterizations are also highly useful for uncertainty estimation.

Another way to aid the convergence of NN models into 'good' minima is to adjust the learning rate of the optimizer:

```{figure} notebooks/learning_curve_val_lrs.png
:alt: "validation learning curves for different learning rates"
:width: 400px
:align: center

Learning curves for validation loss of neural networks trained with different learning rates (2 hidden layers, 64 neurons per layer).
```

Here, a large learning rate of 0.08 leads to a fast initial decrease of the loss, which then stagnates on a relatively high level, while the smaller rates of 0.01 and 0.001 lead to much better models. Here, the smaller learning rate of 0.001 also displays markedly smoother convergence. However, an even smaller rate of 0.0001 leads to a very slow decrease in the validation loss. While this model may also ultimately converge to a low validation error if given enough epochs, this is requires significantly more computational effort.

Similarly, the batch size also influences the generalization error of the models:

```{figure} notebooks/learning_curve_val_batch_size.png
:alt: "validation learning curves for different batch sizes"
:width: 400px
:align: center

Learning curves for validation loss of of neural networks trained with different batch sizes (2 hidden layers, 64 neurons per layer).
```

```{figure} notebooks/val_loss_vs_batch_size.png
:alt: "validation loss for different batch sizes"
:width: 400px
:align: center

Validation loss for different batch sizes (2 hidden layers, 64 neurons per layer).
```
We find an optimal loss for intermediate batch sizes around 64 in this example. This is the result of a number of conflicting effects: Small batch sizes lead to more weigth updates per epoch, which can be a positive thing. On the flipside, a small batch leads to a noisy estimate of the gradient. As a consequence, many of these updates are not optimal in terms of generalization. It is furthermore important to take into account the computational cost of each epoch, which is not constant accross batch sizes:

```{figure} notebooks/timings_batch_size.png
:alt: "timings for different batch sizes"
:width: 400px
:align: center

Time per epoch for different batch sizes (2 hidden layers, 64 neurons per layer).
```

The time per epoch decreases for larger batch sizes, both because fewer backpropagation passes are used in each epoch and because larger batches are more efficient to compute (up to a point). Since we used a constant maximum number of epochs above, the results should be treated with caution. Potentially a better (or equally good) model could have been obtained with batch size 128, if we had trained for more epochs. Nonetheless, 64 seems like a good compromise.

### Architecture

The other source of implicit regularization is the architecture of the network itself. For example, we already saw that the choice of activation function influences the shape of the NNs, particularly in regions where data is scarse. ReLU for example leads to piecewise linear interpolations, which will not fluctuate between datapoints. The very simple linearity of ReLU is thus a form of regularization in itself. Compare this to polynomials, which in principle can also achieve high capacity but become extremely irregular at high orders.

There are also a number of special layers, which can help regularizing NNs. An example is the so-called *dropout* method. Here, a fraction of the connections in the NN are randomly set to zero at each training step. This forces the model to adapt to these changes, leading to more robust models. Another common modification is *batch normalization*. This is essentially an extension of the standardization idea we already discussed, but applied within the network. Specifically, the output of each hidden layer is standardized according to the mean and standard deviation of the given minibatch during training. This mainly has benefits for stabilizing the training of very deep neural networks, but it also has a regularizing effect. We will not discuss these methods furthere here, as they have no benefit for the relatively small neural networks we use here.

Finally, the size of the network can also have a significant influence on the generalization error. As mentioned above, the double descent behaviour is not always observed empirically because dataset sizes are limited. For the folding energy regression, increasing the number of layers and (in particular) the number of neuorns per hidden layer has some benefit:

```{figure} notebooks/val_loss_vs_n_layers.png
:alt: "timings for different batch sizes"
:width: 400px
:align: center

Validation loss for different numbers of hidden layers (64 neurons per layer).
```

```{figure} notebooks/val_loss_vs_n_nodes.png
:alt: "timings for different batch sizes"
:width: 400px
:align: center

Validation loss for different numbers of neurons per hidden layer (2 hidden layers).
```

In this case making the network wider leads to more consistent improvements than making it deeper, somewhat going against the statement in the previous chapter that deep neural networks are preferred over wide ones in many cases. In this case, it seems that the ability to learn hierarchical features is not benefitial for the current problem, however.


## Explicit Regularizers

NNs strongly benefit from implicit regularization. It is, however, also possible to explicitly regularize them. Explicit regularization means adding a term to the loss function, which biases the optimizer towards simpler solutions. A very common method (which we will encounter again later on) is $\mathcal{l}_{2}$ regularization. In this case, the sum of squares loss is expanded as follows:

$$
\mathcal{L} = \sum_{i=1}^{N_\mathrm{train}} (f(x_i)-y_i)^2 + \lambda \sum_{j=1}^{N_{par}} \theta_j^2
$$

Here, the first sum is the familiar sum of squared errors over the training set. The new regularization term is a sum over the squares of all trainable parameters $\theta_j$ (*i.e.* all weights and biases), multiplied by a scaling factor $\lambda$. This means that $\mathcal{l}_{2}$ regularization forces the parameters of the model to be as small as possible. The hyperparameter $\lambda$ specifies the regularization strength. If it is very large, the loss is dominated by the second term and the model will tend towards all parameters being zero. Smaller values lead to a trade-off between minimizing the training error and keeping the parameters small.

At this point you might wonder why keeping the parameters small regularizes the model. This can again be illustrated with polynomial regression. The reason why polynomials often explode when extrapolating beyond their fitting regime is that they have large coefficients with opposite signs that compensate to provide a good fit of the data. However, this compensation does not generalize to other ranges of input values, were the respective terms behave differently. By forcing all coefficients to be as small as possible, this situation is avoided:  

```{figure} notebooks/reg_ensemble1.png
:alt: "an ensemble of functions fitting three datapoints"
:width: 400px
:align: center

An unregularized ensemble of 50 functions which perfectly fit three datapoints but differ from each other strongly otherwise.
```

```{figure} notebooks/reg_ensemble2.png
:alt: "an ensemble of functions fitting three datapoints"
:width: 400px
:align: center

A slightly regularized ensemble of 50 functions, fitted to the same data.

```

```{figure} notebooks/reg_ensemble4.png
:alt: "an ensemble of functions fitting three datapoints"
:width: 400px
:align: center

A strongly regularized ensemble of 50 functions, fitted to the same data.
```

As you can see, increasing the regularization essentially collapses the space of possible functions to a single very smooth fit. Note that this example is somewhat artificial though, as the polynomials in this example are constrained to always provide a perfect fit of the data. In reality, regularization is a matter of balance, as overly strong regularization leads increasing training errors.

In many deep learning frameworks, $\mathcal{l}_{2}$ regularization is implemented as part of the optimizer, instead of the loss function. This is entirely equivalent in terms of the optimal parameters but has the advantage that the loss functions of models with different regularization strengths can be compared directly. In this case  $\mathcal{l}_{2}$ regularization is often referred to as *weight decay*. In our toy model, turning on weight decay has some benefits, here illustrated for models trained on the smallest training set of 500 examples:

```{figure} notebooks/val_loss_vs_weight_decay.png
:alt: "Validation loss for different regularization"
:width: 400px
:align: center

Validation loss for different regularization strengths (2 hidden layers, 256 neurons per layer, 500 training examples). The dashed line indicates the loss of the unregularized model.
```

## Validation and Testing

We have now considered in depth how the hyperparameters of our model and training procedure impact the quality of our models. To this end we extensively made use of the validation loss. Based on these model comparisons, we find that our batch size, learning rate and number of hidden layers were actually quite close to optimal. However, using more neurons per layer, early stopping and weight decay for regularization promises some further improvements. Given the large number of hyperparameters we have considered in this way, there is a danger however, namely that we are actually overfitting on the validation set. It should now become clear why we also kept a separate *test set*. This allows us to judge whether the final model also generalizes to completely unseen data. 

```{figure} notebooks/test_correlation_plot_opt.png
:alt: "predictive accuracy on unseen test set"
:width: 400px
:align: center

Correlation plot showing the predictive accuracy of the test set in the final model.
```

