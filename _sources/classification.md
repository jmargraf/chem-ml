# Classification

Having mostly focused on regression so far, let's turn to another highly popular ML task: Classification. Like a regressor, a classifier learns from labeled training data, with the slight difference that the labels are discrete in classification. In other words, every datapoint is assigned to a particular category and the categories are assumed to be independent from each other. The most common example of this is image classification, i.e. determining whether an image shows a cat or a dog.

## Logistic Regression

Before turning to ML models, it is again instructive to consider the simplest classification method, logistic regression. Consider the following dataset:

```{figure} notebooks/class_data_1d.png
:alt: "1d data"
:width: 400px
:align: center

Binary 1D data.
```

Here, we plot whether a certain material passed a mechanical stress test, based on the length of time it was sintered in the oven. Since the outcome of the stress test is binary (either the material breaks or it doesn't), this data has a kind of step-function distribution. For long sintering times all materials passed, for short ones most failed. Based on this data, we would like to have a model, which tells us the probability that a material will pass the test for a given sintering time. 

To achieve this we can fit a logistic function to the data:

$$
p(x) = \frac{1}{1+\exp(-k(x-x_0))}
$$


This function (which looks suspiciously like the Fermi-Dirac distribution) has the parameters $k$ (determining how sharp the *decision boundary* is) and $x_0$ (determining where the decision boundary lies). Basically, if $x>x_0$ this model assumes that it is more likely than not that the material will pass the test. Here are some trial functions with different parameters:

```{figure} notebooks/class_log_function_1d.png
:alt: "1d data"
:width: 400px
:align: center

Examples of logistic functions.
```

For practical reasons, this function can also be written in a form that is more familiar from linear regression:

$$
p(x) = \frac{1}{1+\exp((mx+b))}
$$

This is useful, e.g. for implementing logistic regression in NN frameworks like PyTorch.

Independent of the functional form, this leaves us with the question of which are the 'best' parameters for the given data. To determine this we need a loss function that gives us a measure for goodness of fit. A common loss function for classification is the *cross entropy loss*:

$$
\mathcal{L}_\mathrm{CE} = \sum_i^{N_\mathrm{train}} \left( -y_{i}\log{p(x_i)} - (1-y_{i})\log(1-p(x_i)) \right)
$$

Since $y_i$ is either zero or one in binary classification, we can also rewrite this as:

$$
\mathcal{L}_\mathrm{CE} = -\sum_{i:y_i=1}^{N_\mathrm{train}} \log{p(x_i)} - \sum_{i:y_i=0}^{N_\mathrm{train}}\log(1-p(x_i))
$$

The parameters that minimize this loss function are called the *maximum likelihood estimate* and lead to the following probability function:

```{figure} notebooks/class_log_reg_1d.png
:alt: "1d data"
:width: 400px
:align: center

Maximum likelihood logistic regression for the 1D dataset.
```

This gives us a pretty good idea for how long we should sinter the material to obtain robust results.

## General Classifiers

Logistic regression is an excellent starting point for working with discrete data, but in ML we are usually interested in high-dimensional data, i.e. with many input features and several different categories. Think, e.g. of image classification, where each pixel represents an input feature and many categories are possible (boat, airplane, cat, dog, book, etc). Furthermore, logistic regression will only find linear decision boundaries (this can be seen from the linear equation in the exponential).

We have already seen that NNs are good in working with high-dimensional non-linear problems, so these are a natural choice to overcome some of these limitations. But how can we generalize logistic regression in such a way that it allows us to work with multiple (i.e. $N_\mathrm{Cat}$) categories? The answer is that we need to replace the logistic function with something that gives us normalized probabilities for any number of outputs. This is typically done with the *SoftMax* function:

$$
p_{k}(\mathbf{x}) = \frac{\exp(f_k(\mathbf{x}))}{\sum_j^{N_\mathrm{Cat}} \exp(f_{j}(\mathbf{x})) }
$$

Let's unpack this: First, we now have a subscript $k$ for the probabilities, because we need a probability for each category. This is a straightforward generalization of the binary case, where we used $p_1(x) = p(x)$ and $p_0(x) = 1 - p(x)$. Second, we have a function $f$ that has an $N_\mathrm{Cat}$-dimensional output, so that $f_k(\mathbf{x})$ is the $k$th output value for the input $\mathbf{x}$. This function can for example be a neural network with $N_\mathrm{Cat}$ output nodes. These raw outputs are often called *scores* for each category. Finally, we need to get from the scores to normalized probabilities. To achieve this, the SoftMax function basically calculates a Boltzmann weight for each category: The scores are put in an exponential and divided by the sum of exponentials for each category. With a neural network and SoftMax function, we can now build classification models for arbitrarily structured data.

Generalizing the cross entropy loss to more categories is equally straightforward:

$$
\mathcal{L}_\mathrm{CE} = -\sum_{k}^{N_\mathrm{Cat}} \sum_{i:y_i=k}^{N_\mathrm{train}} \log{p(x_i)} 
$$

To test this in practice, let's consider one of the classic toy models of ML: The Iris dataset. This contains data on the morphology 150 Iris flowers belonging to three different species. It collects four different morphological features (related to the shape of the flowers), which form the inputs to the models and labels each flower according to the species it belongs to. 

The main challenge with this dataset is that the species don't form neat clusters. No matter how you plot the data with respect to the input features, there's always some overlap between iris versicolor and iris virginica. 

```{figure} notebooks/Iris_dataset.png
:alt: "iris data"
:width: 600px
:align: center

Scatterplots of the Iris dataset with respect to all pairs of the four input features.
```

But this is only a problem for us, since we cannot visualize things in four dimensional space. Can we train an ML model to find a dividing surface for us? We'll do this with a simple neural network, with four input nodes, two hidden layers with eight nodes each, and three output nodes. To get probabilities for each class we apply the softmax function to the output nodes. The resulting network looks like this, for three different inputs:

```{figure} notebooks/classifiers.png
:alt: "classifier nns"
:width: 600px
:align: center

Illustration of a classifier neural network with three different inputs.
```

While the Iris data is hard to classify visually for a human, it's actually not a very difficult task for a machine. Our (very simple) NN can easily classify the training datapoints. But as we know, the real trick is to do well on an unseen test set. To visualize the performance of our classifier for this, we can look at a so called *confusion matrix*:

```{figure} notebooks/class_iris_confusion.png
:alt: "classifier nns"
:width: 400px
:align: center

Confusion matrix illustrating the test set performance of our simple classifier.
```

This shows the actual and predicted classes of all test datapoints (I left out one third of the dataset for testing). Entries on the diagonal indicate that the predicted and actual classes match, off diagonal entries indicate misclassifications. We find that in this case only a single entry is misclassified, for the tricky distinction between iris versicolor and iris virginica.

