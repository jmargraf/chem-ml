# Introduction

## What is Machine Learning?

The term Machine Learning (ML) refers to a class of computer algorithms that can *self-improve* when being presented with *data*. This is what is meant by *learning*. The ML approach is thus quite different from the way 'normal' deterministic algorithms work, which are developed to solve a problem using a fixed set of rules and parameters. Simply put, the key to a good ML model is having representative *reference data* from which the machine can effectively learn underlying patterns. You will find that another common term for the concepts discussed in this lecture is *artificial intelligence* (AI). There is a lot of overlap between ML and AI, but AI is a somewhat broader and less well defined term. I therefore only use the term ML herein. 

ML has been around for over 70 years (since the 1950s). For most of this time, it was mostly an academic discipline, since deterministic algorithms where usually more reliable and accurate. In the last ten years this has changed decisively, however, due to a combination of improved hardware, improved algorithms, and much wider availability of data. This has lead to breakthroughs in several fields, where ML algorithms are now the state-of-the-art. Here are some examples you may have heard about:

### Games

Computer programs that can play games are a classic in ML/AI research. For example, chess programs were developed as early as the 1950s, and many computer games use algorithms to control non-player characters. Early on, these were based on fixed rules of the type *'if this happens, do this'*. This is enough for simple games, but it leads to problems for complex games like chess and (especially) Go. Here, there are simply too many possibilities to foresee. Chess programs overcame this issue to an extent by simply using powerful hardware, which could evaluate millions of possibilities in seconds (i.e. to explore the game tree in a brute-force manner). But for Go, masterful human players could still easily beat the machines. This only changed recently, when ML programs were able to [beat the world-champion in Go](https://www.nature.com/articles/nature16961). Instead of predetermining moves or trying millions of possibilities, these programs were trained by competing against each other, thus learning powerful and unexpected strategies.

```{figure} ../figures/1_intro/go.JPG
:alt: "A Go board."
:class: bg-primary mb-1
:width: 300px
:align: center

A Go board ([Source](https://commons.wikimedia.org/wiki/File:FloorGoban.JPG)).
```

### Image Recognition

Determining what is on a picture is a surprisingly difficult problem for computers. For example, until recently it was impossible to have a computer program that could accurately determine whether a picture contains a dog or a cat. This is a task that a three year old child can solve very reliably. However, it is actually not really understood *how* a human mind makes this distinction. In other words, you can easily say that this is a dog, but not *why*. As a consequence, you also cannot tell a deterministic computer algorithm how it should perform this classification. 

```{figure} ../figures/1_intro/cats.jpg
:alt: "Cats."
:class: bg-primary mb-1
:width: 300px
:align: center

Cat pictures ([Source](https://commons.wikimedia.org/wiki/File:Cat_poster_1.jpg)).
```

This is the kind of problem where ML algorithms shine: It is possible to build a large database of pictures that are labled with 'dog' or 'cat' (i.e. by having humans do this by hand). Based on this, an ML algorithm can *learn* to classify pictures without needing any predefined rules. 

### Natural Language Processing

Natural Language Processing (NLP) refers to algorithms that deal with text and language. NLP models can for example be used to translate texts, determine the sentiments of statements or even produce completely new texts on a topic. Even more so than with images, languages are extraordinarily high-dimensional and complicated, however. Machine translation was therefore for a long time extremely challenging, with deterministic algorithms yielding comically bad results ("I hope you are well" -> "Ich hoffe du bist Brunnen"). Again, the wide availability of data and modern ML approaches have completely changed this, as you can see when using Google Translate or similar services.

### Self-Driving Cars

Driving is another example of a task that most humans can learn to perform reasonably well. Yet it is extremely difficult to write a computer algorithm to do the same. This is because many unforseeable things can happen during a drive: The wheather changes, other drivers behave erratically, animals appear on the road, etc. Just as in Go, it is simply impossible to write down all possible scenarios and make a plan for them. While truly self-driving cars are still not available, there are now functioning prototypes which rely heavily on ML. Here, reinforcement learning models (similar to those used to play games) are combined with powerful image recognition models.

### Some Rules of Thumb

Based on these success stories, we can derive some rules of thumb for when ML can beat 'normal' algorithms:

1. **High dimensional data**: Images consist of thousands of pixels, languages have millions of words, etc. ML algorithms are typically well suited to deal with such high-dimensional problems, because they are able to find efficient representations for them. For low dimensional data, a conventional algorithm can often be more effective than an ML model.

2. **Tasks that are "easy" for a human but difficult for a machine**: All of the above examples are (in some sense) easy for humans. Of course, not everyone is a grand-master in chess or fluent in several languages. But thousands of people are and millions more can learn to perform these tasks reasonably well. However, even expert humans cannot explain how to do these things in sufficient detail to write down a deterministic algorithm. ML algorithms do not require such a detailed explanation, as they can learn to detect similarities and patterns in data *from examples*.

3. **High quality data is available or can be produced**: Since ML models learn from examples, we can only train them if sufficient data is available. Consequently, a big reason for the success of ML language models and image classifiers is the availability of large and high quality datasets. Importantly, this doesn't just mean that we need lots of images to train on. It means that we need lots of *labeled* images. (CAPTCHA)


## Tasks and Paradigms

As we have seen in the previous section, ML covers a lot of ground: from image recognition to text translation to self-driving cars. To bring some structure into this, ML is typically divided into three *paradigms*, each of which covers a number of different *tasks*. A paradigm describes the fundamental principle of how ML models can be derived from data. A task describes a certain type of problem that the ML models are supposed to solve.  

The main paradigms of ML are

1. **Supervised Learning**: A model learns from a training set of labeled examples. *Example*: Classifying Cat and Dog pictures based on a set of labeled images.

2. **Unsupervised Learning**: A model finds patterns in a dataset. *Example*: Clustering plants into types based on information about leaf shapes and colors.

3. **Reinforcement Learning**: A model learns by interacting with an environment. *Example*: Chess program that learns by playing against opponents (or other chess programs).

These paradigms are illustrated below:

```{figure} notebooks/Learning_Paradigms.png
:alt: "Cartoon illustration of learning paradigms."
:width: 600px
:align: center

The three paradigms of machine learning. In supervised learning, a model is built from labeled data. In unsupervised learning, patterns are extracted from unlabeled data. In reinforcement learning, and ML agent learns how to behave in an environment (a policy) by acting in the environment and receiving rewards for desired outcomes.
```

Some typical tasks in each paradigm are explained in the following.

### Supervised Learning

1. **Regression**: A regression model finds the relationship between a continuous (usually high-dimensional) input and a continuous output. This is analogous to a classical curve-fitting scenario. 

2. **Classification**: A classification model assigns data points to discrete categories based on continuous or discrete inputs. This is the task where deep learning methods first became widely used, e.g. in image recognition.

```{figure} notebooks/ML_tasks.png
:alt: "Illustration of common ML tasks"
:width: 600px
:align: center

Illustration of common supervised (regression and classification) and unsupervised (clustering) machine learning tasks.
```

### Unsupervised Learning

1. **Clustering**: A clustering algorithm splits a dataset into groups. Unlike classification, clustering does not require predefined categories and a labeled training set. Instead, patterns in the data are used to form the clusters.

2. **Dimensionality Reduction**: The goal of dimensionality reduction is to take a high-dimensional dataset and transform it into a low(er) dimensional one. The most common applications for this task are compression or visualization. Compression allows storing the most important features of a high-dimensional dataset and discarding the rest. Visualization methods are used to understand and illustrate trends in high-dimensional datasets.

3. **Generative Modeling**: A generative model generates new (synthetic) data-points based on examples. Such models can be used to write text or generate images in a certain style. Generative models often use dimensionality reduction to do this. Note that so-called *conditional* generative models (which generate data with some desired properties) use concepts from both supervised and unsupervised learning.

### Reinforcement Learning

1. **Control**: The main domain of reinforcement learning is the control of robots and other machines (*e.g.* self-driving cars).

2. **Optimization**: Similarly, a reinforcement learning agent can also be trained to improve the conditions of some process based on external feedback.

3. **Data Generation**: For certain types of data, reinforcement learning can also be used to build generative models. This is particularly useful when the data can be generated sequentially, *e.g.* by building sentences word by word. In this case, data generation is simply a special case of controlling an agent that choses the next word based on the previous ones.


## Machine Learning Methods

The *paradigms* of ML basically describe what kind of data is used to train a model and the *tasks* describe what a model should achieve (e.g. group data into clusters or predict a predetermined class for an unknown sample). This leaves the question of how this can be realized in practice. We will cover two kinds of ML methods in this course. The first are *Artificial Neural Networks*, which are used in most cutting-edge ML applications, when data and computational resources are available. The second are so-called *Kernel Methods*, which have some practical advantages in 'small data' applications and are also widely used in chemical ML. We devote a separate chapter to each of these methods, so only a very brief introduction is given below.   

### Artificial Neural Networks

*Artificial Neural Networks* (simply called Neural Networks in the following) are loosely inspired by the working principles of the human brain. They are based on a network of artificial neurons (sometimes called *perceptrons*) that can receive, transform and send data. In a *Feed-Forward Network* (or *Multilayer Perceptron*), these neurons are grouped into layers so that each neuron receives data from the neurons in the previous layer and sends data to the neurons in the subsequent layer.

```{figure} notebooks/Neural_Network_Sketch.png
:alt: "Sketch of a Feed-Forward Neural Network"
:width: 300px
:align: center

Sketch of a feed-forward neural network. The input layer (I) consists of four neurons, followed by four hidden layers of 8 neurons each (H) and an output layer consisting of a single neuron. Note that the neurons are exclusively connected to other neurons in the previous and subsequent layers. The colours map to numerical values from -1 to 1. Each neuron has a *state*, and each connection has a *weight*. Training a neural network simply means adjusting the weights to obtain the desired output.
```

The big advantage of neural networks is that they are incredibly flexible. In fact they can be used to approximate any arbitrary function. The downside is that they are very complex objects with many parameters. As a consequence, they are often called black boxes, meaning that it is hard to understand what they are actually learning from the data. Additionally, this makes training and designing Neural Networks something of an art, which takes a lot of practice to do well. 

### Kernel Methods

Although they are a quite old idea, neural networks were rarely used in mainstream applications until relatively recently, due to their computational and operational complexity. Instead, Kernel methods such as *Support Vector Machines* and *Gaussian Processes* were more common. In simple terms, Kernel methods learn by quantifying the similarity of datapoints. This simple idea can be extended to a wide range of tasks, including regression, clustering and classification. 

```{figure} notebooks/Cat_Dog_Kernel_Matrix.png
:alt: "A Kernel Matrix comparing cats and dogs"
:width: 400px
:align: center

A ficticious Kernel matrix, quantifying the similarity between cats and dogs on a scale from zero to one.
```

Because they only require a proper similarity measure (the *Kernel* function), Kernel models are often easier to develop than Neural Networks. A further advantage is that they can be trained with very little data and are ideally suited for situations were data is collected sequentially. On the flipside, training can become expensive for very large datasets, where neural networks tend to perform better.


## Chemical Applications 

Having (very superficially) covered the main concepts and application areas of ML, we can now turn to chemistry. Here, I provide some interesting recent examples of chemical ML applications, without aiming for completeness. Many more examples (and details) will follow in later chapters.

### Theoretical Chemistry

The fact that accurate quantum chemical calculations are computationally expensive is a constant annoyance in theoretical chemistry. In practice this forces researchers to use lower levels of theory and empirical approximations, in order to perform the kinds of simulations they want to do. ML models have emerged as a very powerful tool in this context, as they can be trained on a limited number of reference calculations and subsequently used in long dynamics simulations or to screen very large databases.

```{figure} notebooks/chemicalMLcartoon.png
:alt: "Regression Workflow for Quantum Chemistry"
:width: 600px
:align: center

Illustration of a simple regression workflow with quantum chemical data. An ML model is trained on a *labeled* dataset, where each label represents an expensive quantum chemical calculation. Subsequently, much faster predictions of the target property can be made for unknown molecules.
```

The main application of this approach is the development of [highly accurate interatomic potentials](https://www.science.org/doi/10.1126/sciadv.1701816) (i.e. force fields). From an ML perspective these are simply regression models trained on quantum mechanical energies and forces. However, there are also models that can predict many other molecular and materials properties, such as [NMR chemical shifts](https://www.nature.com/articles/s41467-018-06972-x) or dipole moments.

### Retrosynthesis

Automatic retrosynthesis tools have been around at least since the 1990s, initially focused on fixed rules and reaction templates. More recently ML enhanced tools that use reinforcement learning to efficiently navigate the large range of possible reaction routes. An example of a retrosynthetic route proposed by the [AiZynthFinder](https://github.com/MolecularAI/aizynthfinder) tool is shown below.

```{figure} notebooks/bufotein_retrosynthesis.png
:alt: "AI generated retrosynthesis"
:width: 700px
:align: center

Machine learning predicted retrosynthesis of the natural product bufotein (generated using *AiZynthFinder*).
```

ML-based retrosynthesis algorithms can already be used as a tool for synthetic chemists to explore potential synthesis routes. A potential future prospect is that they can also be combined with automated synthesis robots, leading to completely autonomous synthesis platforms. [Prototypes of such workflows](https://rxn.res.ibm.com/) are currently being developed.

### Optimization

Chemists are often (perhaps unknowingly) faced with optimization problems. What are the best reaction conditions for a synthesis? Which catalyst or solvent should I use? How can I design a molecule with desired properties? The difficulty is that these questions depend on a variety of parameters and it is generally impossible to test all possible combinations. In practice, such optimization problems are therefore often ignored and approaches that work reasonably well are used without exploring whether they could be improved. Even worse, potentially promising approaches are not pursued because the initial parameters were unfavourable.

ML-based optimization and experimental design approaches such as [Bayesian Optimization](https://scikit-optimize.github.io/stable/auto_examples/bayesian-optimization.html#sphx-glr-auto-examples-bayesian-optimization-py) have emerged as a powerful tool in this context. Here, an ML model is iteratively refined based on experimental observations and used to select the next set of parameters to try. This allows a highly efficient and targeted exploration of the parameter space.

```{figure} notebooks/Bayesian_Optimization.png
:alt: "Bayesian Optimization Sketch"
:width: 800px
:align: center

Illustration of a *Bayesian Optimization* algorithm, which searches for the minimum of a fuunction (blue) guided by a machine learning model. Based on three random samples (N=3) an initial approximation of the target function is learned (red curve). Based on this approximation (and the predicted uncertainty, shown as a red shade) new points are selected based on their expected value (the lower the better) and predicted uncertainty (the higher the better). With this strategy, a point close to the global minimum is found with just six function evaluations. 
```

### Structure Prediction

One of the greatest challenges in both experimental and theoretical chemistry is determining the structure of complex molecules, aggregates and solids. From a theoretical point of view, this is often approached as a high-dimensional global optimization problem (which can also be solved with the help of ML, see above). From an experimental perspective, structural determination requires expensive and challenging experiments and/or the interpretation of incomplete information. Either way: determining or predicting chemical structures is difficult and expensive.

There has therefore recently been large interest in ML models that can directly predict the three dimensional structures of chemical system. The most prominent example of this approach is the [AlphaFold2 model](https://www.nature.com/articles/s41586-021-03819-2), which can reliably predict protein structures from the protein sequence alone. In just single year since its announcement, AlphaFold2 has caused a revolution in structural biology.  

In later chapters, we will work with a very simplified version of the protein folding problem based on binary, two-dimensional polymers. 

```{figure} notebooks/Tinyfold_Teaser.png
:alt: "Tinyfold Teaser"
:width: 800px
:align: center

A simple two-dimensional [protein folding model](www.tinyfold.xyz). Changing the sequence of the atom-types leads to very different folding behaviour. 
```

