# Linear regression

You have almost certainly encountered linear regression in your studies before. Very often, this term is used synonimously to fitting a line to some data. In this chapter, we will learn that linear regression is much more than just fitting lines. Instead, linear models are very versatile and form the basis of all the sophisticated machine learning methods we discuss later on. We will also start to introduce some important mathematical concepts and notations, that are used throughout the book.

A quick note: The introduction to linear regression you find here is probably somewhat different from the one you might find on [Wikipedia](https://en.wikipedia.org/wiki/Linear_regression) or other sources. This is because linear regression is first and foremost a method used and developed in the statistics community. The statistics literature focuses much more on uncertainty and other probabilistic aspects, which we mostly ignore for now. This is not because they are unimprotant or uninteresting. However, for time reasons I want to focus on the aspect that is most relevant to ML, which is fitting data and making point predictions. We will get back to uncertainty estimates and probability distributions later on. 

## Basics 

In the previous chapter we established that the goal of regression means finding a mathematical relationship between continuous input and output variables. In the simplest case, both the input and the output are one-dimensional variables, which we will call $x$ and $y$. Let's assume that we have some data in this space, which we will designate as

$$
\mathcal{D}=\{(x_i,y_i)\}^N.
$$

This notation indicates that there is a *set* ($\{ \}$) contaning $N$ specific pairs of $x$ and $y$ values. These pairs are often called *tuples*. The index $i$ is a counting variable which numerates these datapoints. We may for example have six data-points ($N=6$) so that the set has the form

$$
\mathcal{D}=\{(x_1,y_1),(x_2,y_2),(x_3,y_3),(x_4,y_4),(x_5,y_5),(x_6,y_6)\}.
$$

Here is a plot of such a dataset:

```{figure} notebooks/lin_reg_data_1d.png
:alt: "Data 1D"
:width: 400px
:align: center

Plot of a simple one-dimensional dataset.
```

Just from looking at the data, we see that there appears to be a roughly linear relationship between $x$ and $y$. To quantify this relationship, we can fit a line to the data. Mathematically, a line is expressed as:

$$
f(x)=mx+b,
$$

where $m$ is the *slope*, $b$ is the *intercept*. These are the *parameters* which define the function. We can look at some possible lines that can be defined in this manner: 

```{figure} notebooks/lin_reg_data_1d_lines.png
:alt: "Data 1D"
:width: 400px
:align: center

Linear functions with slope $m$ and intercept $b$.
```

Clearly, some of these run closer to the data than others, but none can be considered a good fit.

## Optimizing the parameters

The goal of a regression task is therefore to determine those parameters ($m$ and $b$) that yield a function which best fits the data. Mathematically this is a optimization problem and there are a number of ways to achieve this. However, before we can find these optimal parameters we have to decide how we want to measure the quality of our fit. To do this, we define a so-called loss function $\mathcal{L}$. Simply put,  $\mathcal{L}$ compares a function to the known data and outputs a number. The lower this number is, the better the fit. The most popular loss function for regression is the *sum of squares loss*, defined as:

$$
\mathcal{L} = \sum_{i=1}^{N} (f(x_i)-y_i)^2
$$ (SSE)

In words, this function iterates over all $N$ datapoints and subtracts the value of our function at the corresponding input values ($f(x_i)$) with the known output values $y_i$. If the function is a good fit, this difference will be small. The differences for all datapoints are squared and subsequently summed up. The squaring step has two purposes: First, it makes all differences positive. This is important, because the errors of different datapoints could otherwise compensate (if one is positive and the other negative). This could lead to a situation where the function has a big error for all datapoints but the loss is still zero. Secondly, squaring also makes the optimization of the loss function easier, as we shall see later. There is also a closely related loss function called the *mean squares loss*. Here the expression in Eq. {eq}`SSE` is divided by $N$. This has the advantage that the loss function is then comparable for differently sized datasets, but it does not affect the optimal parameters.

### Grid Search and Random Search

One way to find out the optimal parameters is to do a brute-force grid search. This means that we define a regular grid of values in some interval for $b$ and $m$. Looking at the plot above, we can say that the optimal values probably lie in the intervals $[0.5,2.0]$ for $m$ and $[-0.5,0.5]$ for $b$. We can then construct a uniform grid with, *e.g.*, five points for each parameter, leading to 25 pairs of $m$ and $b$. Then we evaluate the loss function $\mathcal{L}$ for each parameter-pair, which gives us a good idea of what the optimal values should look like. Let's visualize this:

```{figure} notebooks/lin_reg_gridsearch.png
:alt: "Data 1D"
:width: 400px
:align: center

Contour plot of the lossfunction ($\mathcal{L}$) in dependence of $m$ and $b$. The grid values for which $\mathcal{L}$ was evaluated are shown in red, the contours are obtained through interpolation.
```

This contour plot is like a map of the fit quality. We can see that the minimum of $\mathcal{L}$ lies somewhere around $m=1.6$ and $b=0.2$. However, we can't really be sure what the optimal values are, since out grid is fairly coarse and the minimum is rather flat. Another way to visualize this is to plot all the lines, along with the data:

```{figure} notebooks/lin_reg_gridsearch_line.png
:alt: "Data 1D"
:width: 400px
:align: center

Plot of all linear functions sampled in the gridsearch. The best fitting line is shown in red.
```
This shows that our loss function indeed captures what we wanted: The best fitting line (in red) is also visually a good match to the data. But this plot also indicates some of the downsides of gridsearching. You can see that there are lots of other possible lines that we didn't try, which could in principle be a better fit to the data. We also tried lots of lines that were really bad fits. A grid search thus neither provides a very precise answer nor is it efficient. Perhaps surprisingly, picking 25 random points on this map would actually have been a slightly better alternative. This is particularly true if the loss function is more sensitive to one of the parameters than the other. You can see that our regular grid only samples five different values for $m$ and $b$ respectively, while a random search would sample 25 different values for each. If the loss mostly depends on one of the parameters, we therefore get a more precise answer from the random search, as illustrated below:

```{figure} notebooks/lin_reg_gridvsrandom.png
:alt: "Data 1D"
:width: 600px
:align: center

Grid (left) and random (center) search patterns for a loss function which strongly depends on one of the variables, but not on the other one. Here, random search is benefitial, because it samples more values of the important variable, even though the coverage of the space is not as uniforms. On the right, a *quasi-random* [Sobol sequence](https://en.wikipedia.org/wiki/Sobol_sequence) is shown, which combines the advantages of both.
```

The bigger problem with both grid and random searches is that they do not scale to higher dimensions, however. You probably already noticed that our grid had $N_{g}^2$ (i.e. 25) points. For functions with $d$ parameters, a grid with the same resolution requires $N_{g}^d$ points. This means that we need 125 points for three parameters and 3125 points for five parameters. This is the famous *curse of dimensionality*.

Overall, grid and random searches are therefore only used in low dimensional problems. Even there, they are kind of a last resort, since better alternatives for finding the minimum are almost always available. We will discuss some of them next.

### Gradient-Based Optimization

Gradient-based methods are a very popular and conceptually simple class of optimization algorithms. We will consider the simplest of these methods, which is called Gradient Descent (GD). The intuition behind GD is that we can use the derivatives (gradients) of the loss function $\mathcal{L}$ to determine in which direction we should modify our parameters to decrease the loss. Starting from some initial values $m_0$ and $b_0$, we can thus get better values according to:

$$
m_1 = m_0 - \gamma\frac{\partial \mathcal{L(m_0,b_0)}}{\partial m}
$$

and

$$
b_1 = b_0 - \gamma\frac{\partial \mathcal{L(m_0,b_0)}}{\partial b}.
$$

Notice that I used the notation $\mathcal{L(m_0,b_0)}$ here, to emphasize that the loss is a function of the parameters, and that we are evaluating the gradient at the point $(m_0,b_0)$. You should note that the gradient mainly tells us in which direction the loss will decrease. It also stands to reason that a large gradient should lead to a larger change in a parameter. But how large exactly? This is what the parameter $\gamma$ in the above equations controls. In the ML context this is usually called the *learning rate*. For very small values of $\gamma$, the loss is guaranteed to go down at each step. On the flipside, this might mean that we have to take many small steps to get to the minimum. A large learning rate can also lead to problems, however, since a step might then actually lead to an increase in energy and/or oscillatory behaviour. 

Using the same formula, we can get even better values $m_2$ and $b_2$ by looking at the gradient at $(m_1,b_1)$, and so on. GD thus allows us to explore the loss function landscape in steps, kind of like a snowboarder riding down a mountain. Let's see how this works in practice for our dataset. 

To this end, we first need mathematical expressions for the partial derivatives $\frac{\mathcal{L(m,b)}}{\partial m}$ and $\frac{\mathcal{L(m,b)}}{\partial b}$. These are easy to compute from {eq}`SSE`, using the chain rule:

$$
\frac{\partial \mathcal{L(m,b)}}{\partial m} = \sum_{i=1}^N{2(mx_i+b-y_i)x_i}
$$ (SSE_partial_m)

and

$$
\frac{\partial \mathcal{L(m,b)}}{\partial b} = \sum_{i=1}^N{2(mx_i+b-y_i)}.
$$ (SSE_partial_b)

With these expressions in hand, the only thing missing is to define some starting values for $m$ and $b$. In principle, we already know more or less what these values should be from the grid search. I will choose some rather bad starting point ($m_0=0.5$ and $b_0=0.0$) instead, since it makes a more interesting illustration. Below you find a visualization of 25 gradient descent steps using $\gamma=0.1$

```{figure} notebooks/lin_reg_grad_descent_map.png
:alt: "Data 1D"
:width: 400px
:align: center

Contour plot of the lossfunction ($\mathcal{L}$) in dependence of $m$ and $b$. The values for which $\mathcal{L}$ was evaluated in the gradient descent run are shown in red.
```

We can also visualize the individual lines sampled during the GD run.

```{figure} notebooks/lin_reg_grad_descent_line.png
:alt: "Data 1D"
:width: 400px
:align: center

Plot of all linear functions sampled in the gradient descent run. The best fitting line is shown in red.
```

Both of these figures are strickingly different from the grid search example. The GD best fitting line is clearly better than the one we found in the grid search, even though we tried the same number of parameter combinations. This is because the gradient information allowed us to mainly sample close to the minimum and further refine good combinations. As a consequence, most of the GD lines are fairly close to the data, which was not the case for the grid. However, there is also a notable oscillation in the first couple of GD steps. This is related to the larening rate $\gamma$. Consider the same optimization with a smaller rate of $\gamma=0.05$:


```{figure} notebooks/lin_reg_grad_descent_map_slow.png
:alt: "Data 1D"
:width: 400px
:align: center

Contour plot of the lossfunction ($\mathcal{L}$) in dependence of $m$ and $b$. The values for which $\mathcal{L}$ was evaluated in the gradient descent run are shown in red.
```

This removes some of the osscilation but also causes a slower approach to the minimum. For a smooth function like the one considered here, both choices ultimately bring us to a good solution. Importantly, GD is only one example of a large variety of gradient based optimizers. It is in fact the simplest possible one, and in practice more sophisticated methods are often used (e.g. of the Conjugate Gradient or Quasi-Newton variety). These may include second-derivative or momentum information, which is particularly helpful for quicker convergence close to the minimum.

A big advantage of gradient based methods in general is that they can also be applied in high-dimensional cases. They are therefore very widely used in ML. There are some caveats to keep in mind though: First, gradient methods can only be applied for continuous, smooth functions. Secondly, they will usually find the closest minimum to your starting guess, but not necessarily the *global minimum*. Third, they (obviously) require gradients. More precisely, the gradients should be accurate and cheap to compute. Since the latter is not always the case, there are also some gradient-free optimization methods. These will not be discussed herein.

### Closed-Form Solution

A special feature of linear methods is that they offer another powerful way of getting the optimal parameters, namely via a closed-form mathematical expression. In other words, we can derive a more or less simple formula that gives us the exact parameters $(m_\mathrm{min},b_\mathrm{min})$ without having to experiment with different possibilities at all. To understand how this works, we recall that a minimum of a function is characterized by the fact that the gradients are zero at this point. We can therefore simply take the partial derivatives in {eq}`SSE_partial_m` and {eq}`SSE_partial_b` and set them to zero. When you do this you notice that you can directly solve for $m_\mathrm{min}$ and $b_\mathrm{min}$, respectively:

$$
m_\mathrm{min}=\frac{\sum_{i=1}^{N}(2y_ix_i-2b_\mathrm{min}x_i)}{\sum_{i=1}^{N}2x_i^2}
$$

and 

$$
b_\mathrm{min}=\frac{\sum_{i=1}^{N}(2y_i-2m_\mathrm{min}x_i)}{2N}.
$$

Now we insert the second expression into the first one and (after some manipulations) get:

$$
m_\mathrm{min}=\frac{\sum_{i=1}^{N}{y_ix_i}-\tfrac{1}{N}\sum_{i,j=1}^{N}{y_jx_i}}{\sum_{i=1}^{N}{x_i^2}-\tfrac{1}{N}\sum_{i,j=1}^{N}{x_jx_i}}
$$

This expression looks a little cumbersome, but the important thing is that it only depends on the datapoints ($x_i$ and $y_i$). We can thus directly calculate $m_\mathrm{min}$ and insert the result into the equation for $b_\mathrm{min}$. This is what closed-form solution means. Below, I plot the resulting line, comparing with our previous results from grid search and GD:

```{figure} notebooks/lin_reg_compare_opt.png
:alt: "Data 1D"
:width: 400px
:align: center

Comparing optimal fits obtained with grid search, gradient descent and the closed form solution.
```

You can see that the GD and closed-form solutions are very close. By running GD long enough with a small learning rate we can, in fact, get arbitrarily close to the closed-form parameters. There are some important things to note here: The formulas that we derived for $m_\mathrm{min}$ and $b_\mathrm{min}$ are *universal*. This means that we can use them to fit lines to any dataset, no matter how big or small (as long as it contains at least two datapoints, that is). Moreover, we are always guaranteed to get the best possible values for $m_\mathrm{min}$ and $b_\mathrm{min}$, in the sense that they have the lowest possible sum-of-squares loss. 

This raises the question why we should bother with the other optimization methods at all. There are two reasons: Most importantly, closed-form solutions do not exist for all types of models and loss-functions. As I wrote above, this is in fact something of a special property of linear models. Even for linear models, closed-form solutions are not available for most loss functions. Consider a simple modification of {eq}`SSE`, where instead of squaring the error we take its absolute value:

$$
\mathcal{L} = \sum_{i=1}^{N} |f(x_i)-y_i|
$$ (SAE)

This is a perfectly valid loss function, which is zero for a perfect fit and positive in all other cases. However, there no longer exits a closed form solution for fitting a line that minimizes this function.

The second reason why we might want to rely on other optimization methods is that in some cases closed-form solutions exits but they are computationally inconvenient to calculate. This can e.g. be the case for linear models with many parameters.

## More than fitting lines

I initially promised that linear regression is about more than fitting lines to data. Now we have spent a lot of time doing just that. But hopefully, this has given us the background necessary to tackle more interesting problems with linear regression. We begin by fitting a little more complex data.

### Polynomial Features and other Basis Functions

Consider the following dataset:

```{figure} notebooks/lin_reg_data_1d_nonlin.png
:alt: "Data 1D"
:width: 400px
:align: center

A non-linear dataset.
```

Clearly a line will not provide a satisfactory fit here, because the $y$-values do not even approximately rise (or fall) linearly with $x$. However, all is not lost for linear regression in this case. Just because $y$ is not linear in $x$, does not mean that it cannot be linear in some other variable (or combinations thereof). This is a very important point, so it is worth repeating in other words: The term *linear* in linear regression does not refer to the fact that we are using lines to fit data. It refers to the fact that the functions we use to fit are *linear combinations* of terms whose coefficients we optimize. The linear function we have been using so far is just a special case. The general form of these linear combinations is:

$$
f(x) = c_1\phi_1(x) + c_2\phi_2(x) + c_3\phi_3(x) + c_4\phi_4(x) ...
$$

or, in more compact notation:

$$
f(x) = \sum_{j} c_i\phi_j(x).
$$

In the examples so far, we have used $c_1=m$, $\phi_1=x$, $c_2=b$ and $\phi_2=1$. Returning to our new dataset, this raises the question what kind of other terms we could include in the fitting function. The answer is simple: Any conceivable function of $x$ can be used. We call these functions *basis functions*, a term you may be familiar with from quantum chemistry. We will get to some specific examples of basis functions soon, but first we need to update our terminology and notation.

From here on out we will have more and more parameters to fit. This makes it inconvenient to carry them around as different variables (e.g. $m$ and $b$). We will therefore store them in a *coefficient vector* $\mathbf{c}$, with the elements $c_1, c_2, ..., c_{D}$ (for a model with $D$ basis functions):

$$
\mathbf{c} =
\begin{pmatrix}
c_{1}  \\
c_{2}  \\
\vdots   \\
c_{D}  \\
\end{pmatrix}
$$

These coefficients are sometimes also called *regression weights*, because they quantify the impact of a given basis function on the prediction. We also have to change the way we handle our data. So far we were using pairs (tuples) of values $x_i$ and $y_i$ to describe our dataset. Since we are now fitting our data in terms of multiple terms, we get multiple *features* $x_{ij}$ so that a single datapoint is now described in terms of $(x_{i1},x_{i2},...,x_{iD},y_i)$. The individual features can be computed from the basis functions and the "raw" input variable $x_i$: 

$$

x_{ij} = \phi_j(x_i)

$$ 

The job of the basis functions is therefore the *featurization* of the raw input variables. In particular, the basis functions perform a non-linear transformation of the input. This is how *non-linear* data can be treated with *linear* regression: All the non-linearity is taken care of by the featurization step and all fitting parameters $c_j$ remain linear. This is why linearity is actually not such a strong restriction. As long as we have the right basis functions, we can fit anything with linear regression.

Just as with the coefficients $\mathbf{c}$, we will also use vectors and matrices to handle the data. In particular, we define the *feature matrix* $\mathbf{X}$ and the *target vector* $\mathbf{y}$ as:

$$
\mathbf{X} =
\begin{pmatrix}
x_{1,1} & x_{1,2} & \cdots & x_{1,D} \\
x_{2,1} & x_{2,2} & \cdots & x_{2,D} \\
\vdots  & \vdots  & \ddots & \vdots  \\
x_{N,1} & x_{N,2} & \cdots & x_{N,D} \\
\end{pmatrix}
$$

and 

$$
\mathbf{y} =
\begin{pmatrix}
y_{1}  \\
y_{2}  \\
\vdots   \\
y_{N}  \\
\end{pmatrix}
$$

This switch to vectors and matrices is not just a matter of notational convenience. Importantly, it also allows us to formulate the regression task in terms of *linear algebra*, *i.e.* the language of matrix multiplications and the like. Indeed, linear algebra is the mathematical foundation behind most of ML and is essential for the computationally efficient implementations of ML algorithms. The linear regression problem can thus be formulated in terms of a matrix-vector multiplication, for example:

$$
\mathbf{X}\mathbf{c} \approx \mathbf{y}
$$ (lin-alg-reg)

In words: we are looking for a $D$-dimensional vector $\mathbf{c}$ which, when multiplied with the $N \times D$-dimensional feature matrix $\mathbf{X}$, yields the best possible approximation to the $N$-dimensional target vector $y$. Again, we define the "best" approximation in terms of the sum-of-squares loss function. In linear algebra notation, this reads:

$$
\mathcal{L} = ||\mathbf{X}\mathbf{c} - \mathbf{y}||^2
$$

We could now use gradient descent to optimize the coefficients in $\mathbf{c}$. However, as before, we can also get the closed-form solution directly. Taking the derivative of $\mathcal{L}$ with respect to $\mathbf{c}$ we obtain:

$$
\frac{\partial \mathcal{L}}{\partial \mathbf{c}} = 2\mathbf{X}^T (\mathbf{X} \mathbf{c} - \mathbf{y}),
$$

which we set to zero and solve for $\mathbf{c}$ to obtain:

$$
\mathbf{c} = (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{y}
$$ (Closed-form-OLS)

Let's take a moment to digest this. Equation {eq}`Closed-form-OLS` allows us to calculate the so-called *least-squares* solution to any linear regression problem. We derived it in a very similar way to the closed-form expressions for $m$ and $b$ above, but we now have a single formula that we can use no matter how many parameters we are using. Note that there are some caveats, however: To use this expression, the number of datapoints $N$ should be larger than the number of features $D$. Otherwise, there are typically infinitely many solutions for $\mathbf{c}$ (unless we have chosen bad basis functions and/or the data has inconsistencies) that exactly fit the data. You may remember this from discussing over- and underdetermined systems of linear equations in math class. Indeed, equation {eq}`lin-alg-reg` is just that: a system of linear equations. Mathematically, the question of whether a least-squares solution exists comes down to the invertibility of $\mathbf{X}^T \mathbf{X}$.

With our new linear algebra machinery in place, the question now is: Which and how many basis functions should I use to fit the data. A very simple choice is to use a polynomial basis. This means, that we take our input $x_i$ and compute features $x_i^0,x_i^1,x_i^2,x_i^3,...,x_i^{D-1}$. Indeed, our previous line-fitting model was just a special case of this approach, using the first two features of this series (*i.e.* $D=2$). This is how the fits of our new dataset look with polynomial features:

```{figure} notebooks/lin_reg_data_1d_nonlin_fits.png
:alt: "Data 1D"
:width: 400px
:align: center

Fitting polynomials of different orders to a non-linear dataset.
```

Clearly, increasing the dimensionality of our regression model has some benefit. With higher order polynomial features, the fit to the data improves. But how many features should we use? The answer to this question depends somewhat on what our goal is. As mentioned above, we can in priciple fit the data perfectly by using a high-order polynomial. Generally speaking, this is usually not the purpose of an ML model, however. Instead, we want to fit the data in order to make *predictions*. This means that we want to use the regression model at values of $x$, where the corresponding value of $y$ is unknown. Instead of just looking at the loss of our fit, we should therefore check how well the model performs for unknown data. Alas, the problem with unknown data is, of course, that it is unknown.

A simple way around this problem is to use *data splits*. This means that we divide our data into several subsets, in the simplest case a *training set* and a *validation set*. An example of this (using a random sample of 5 as a validation set) is shown below:

```{figure} notebooks/lin_reg_data_1d_nonlin_train_val.png
:alt: "Data 1D"
:width: 400px
:align: center

Splitting data into a training and validation set.
```

Now, we can fit a model to the training data and check its predictions for the validation data. This is shown in the next figure for polynomial fits using different values of $D$. Here I used ten randomly drawn validation sets (of 5 datapoints) and report the average loss:

```{figure} notebooks/lin_reg_train_val_loss.png
:alt: "Data 1D"
:width: 400px
:align: center

Training and validation errors for polynomial fits to non-linear data.
```

This shows that the training error continuously decreases as we increase the number of polynomial features, as expected. The behaviour of the validation error is more erratic, however. We can see that it is large when $D$ is small and subsequently reaches a plateau of small loss values for $D$=7-9. However, as we increase $D$ further, the validation error begins to increase, even as the training error still decreases. The reason for this is that we begin to *overfit* on the training data. This is best illustrated by comparing two fits: 

```{figure} notebooks/lin_reg_over_underfit.png
:alt: "Data 1D"
:width: 400px
:align: center

Fits using 7 and 14 polynomial features.
```
Here, the black line corresponds to a good fit, with essentially the same training and validation error. This means that the model fits known and unknown datapoints equally well, which is the ideal situation. In contrast, the cyan line fits the training data perfectly but osccilates wildly in between. Even without looking at the validation datapoints, it would be clear that the black line is a better fit than the cyan one, simply because it is more *regular*. We will return to this concept later on. One last thing to note is that the above results are not invariant to how the training/validation split is performed. I could, *e.g.*, have chosen just a single point for validation or ten. It also makes a difference whether the validation datapoints are evenly distributed over the range (*interpolation*) or lie outside the training range (*extrapolation*). Ultimately, the procedure for generating the validation set should be related to the application that is intended for the regression model. We will discuss this in more detail later on.


## Regression in Higher Dimensionional Spaces

We have now seen that linear regression can be used to fit non-linear data in a one dimensional space by creating a higher-dimensional featurization of the input variable. It should therefore not be too surprising that the exact same formulas can also be applied to higher-dimensional spaces. When we talk about higher dimensions, this can, of course, mean spatial dimensions. But more generally, we may have all kinds of data with multiple input variables. For example, in time-resolved spectroscopy, one records the intensity as a function of both the wavelength and the time-delay. In an automated synthesis framework, we might be controlling the yield of a reaction by varying reaction time, temperature and multiple concentrations. All of these are high-dimensional settings.

While the mathematical formulas we discussed above apply equally in higher dimensions, some practical difficulties arise. Consider the following two dimensional function:

```{figure} notebooks/lin_reg_2D_data.png
:alt: "Data 2D"
:width: 400px
:align: center

A two-dimensional function. Red dots mark discrete datapoints used in the fits below.
```

You may have noticed that the axis labels here are $x_1$ and $x_2$. We will stick to this notation for higher dimensional functions throughout this book: The input variables are always $x_{i}$, the outputs always $y_{i}$ (in this case just a single number $y$). In this situation, we have 1000 datapoints and there is no noise on the function. We should thus in principle be able to fit this more or less exactly. The question is, however, what basis functions we can use. A popular choice in higher dimensional spaces are *radial basis functions* (RBFs) also known as *Gaussian functions*:

$$
\phi_j(\mathbf{x}) = \exp \left( \frac{-||\mathbf{x}-\mathbf{x_j}||^2}{\sigma_j} \right)
$$

Unlike polynomial features, RBFs require two additional parameters, namely a *location* $\mathbf{x_j}$ and a *width* $\sigma_j$. If we do not have any additional information about the function we are fitting it makes sense to distribute the basis functions on a uniform grid and assign the same width to all of them. The width should furthermore be large enough so that the functions overlap somewhat (i.e. to cover all space) but not too large, since the basis functions are then unable to reproduce finer aspects of the data. In practice, we could again use a validation set to determine $\sigma$, but we'll simply use a constant value of 1.0 for now, which turns out to be good enough. 

One issue with higher dimensional functions (in particular, with three and more dimensions), is that it becomes difficult to judge the quality of the fit by looking at the data. Instead, it can be helpful to look at the *correlation* between predicted and actual values of $y$. This is shown below for two fits, using 16 and 64 evenly distributed Gaussians.

```{figure} notebooks/lin_reg_2D_parity_plot.png
:alt: "Data 2D"
:width: 400px
:align: center

A parity (or correlation) plot for linear fits of the 2D function, using a radial basis with 16 and 64 Gaussians.
```

And here are the 2D plots:

```{figure} notebooks/lin_reg_2D_models.png
:alt: "Data 2D"
:width: 400px
:align: center

Fits to the two-dimensional dataset. Basis locations are marked with white crosses.
```

We can see that the smaller basis is unable to reproduce the data very accurately, whereas the fit is excellent with 64 Gaussians. This already indicates a common phenomenon, comparing to the 1D cases above: We need a lot more data and basis functions for 2D than for 1D. And thanks to the *curse of dimensionality* this keeps getting worse and worse. However, we do not actually need so many basis functions, in principle. In fact, this data was generated using three Gaussians with different width. It would therefore be possible to fit the data exactly using just three basis functions. The lesson here is: fitting high-dimensional data with linear models and raw input features or generic basis functions is usually inaccurate and/or inefficient. So why don't we optimize the width and location of the basis functions? 

We could in principle do this. The problem is that our regression model is not *linear* with respect to the parameters $\mathbf{x}_j$ and $\sigma_j$ (because they are inside of the exponential). This means that there is no closed-form solution to this optimization problem and we would need to do a high-dimensional non-linear optimizations. In the world of linear regression we are thus stuck, if we are unable to define adequate basis functions. The ML techniques discussed later in this course will tackle this fundamental problem in different ways. 


## Tensors

So far we have considered multidimensional inputs. We can further generalize linear regression to in- and outputs of arbitrary shapes. To this end we introduce *tensors*. In ML, a tensor is usually simply understood as a multidimensional array. The dimensionality of a tensor is defined by its order. A first order tensor is thus equivalent to a vector, a second order tensor to a matrix. A third order tensor can be visualized as a stack of matrices, just like a matrix is a stack of vectors:

```{figure} notebooks/tensors.png
:alt: "Data 2D"
:width: 600px
:align: center

Illustration of tensors of first order, second order and third order.
```

Tensors may sound exotic, but they are actualy highly useful in data science. For example, images can naturally be encoded as matrices (or rather as $N_\mathrm{pixels} \times N_\mathrm{pixels}$ rank 2 tensors). When building a regression model based on $N_\mathrm{data}$ images, the entire dataset can be encoded as a rank 3 tensor of dimensions $N_\mathrm{data} \times N_\mathrm{pixels} \times N_\mathrm{pixels}$.

To build a linear regression model for this dataset, we can define a general linear transformation (called a *tensor contraction*) between two tensors $\mathbf{X}$ and $\mathbf{Y}$, analogous to a matrix multiplication:

$$
\mathbf{X} \mathbf{C} = \mathbf{Y}
$$

Just as with matrix multiplication, we have to make sure that the dimensions of the tensors match. A common case would be that $Y$ is a first order tensor of shape $N_\mathrm{data}$ (*i.e.*, it assigns a single scalar to each image in the dataset). Then, the coefficient tensor $C$ must have the dimensions $N_\mathrm{pixels} \times N_\mathrm{pixels}$. Similarly, if $Y$ is a second order tensor of shape $N_\mathrm{data} \times N_\mathrm{out}$ (i.e., it assigns a vector to each image in the dataset), the coefficient tensor $C$ must have the dimensions $N_\mathrm{pixels} \times N_\mathrm{pixels} \times N_\mathrm{out}$. We will return to tensor operations in later chapters. For now, the most important thing to keep in mind is that linear operations can be used to map tensors of different shapes to each other.



## Key concepts

```{admonition} Linearity
Refers to the fact that an equation is linearly dependent on a certain variable
```

```{admonition} Features
The features of a dataset are the raw input data and/or some non-linear transformation of that input data.
```

```{admonition} Basis Functions
Basis functions perform some well defined non-linear transformation of the input features, in order to generate new features. This allows describing a non-linear relationship between $x$ and $y$ using linear regression. Prominent examples are polynomial and radial basis functions, but many other types exist.
```

