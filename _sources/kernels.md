# Kernel Methods

With Neural Networks we have now learned about one of the currently most popular ML framework. Despite all their advantages, there are also some downsides to NNs, however. Most importantly, NN models are in many ways complicated to set up, as we saw in the last chapter. There are many hyperparameters to take into account, regarding the model architecture and training procedure. Furthermore, NNs are often 'data-hungry', meaning that they only truly develop their potential when a lot of data is available. In this chapter, we will therefore focus on a complementary ML framework, so called Kernel methods.

To get there, we first briefly revisit linear regression, taking into account what we've learned about regularization.

## Regularization in Linear Models

As we've discussed, NNs can be understood as *representation learners*. In other words, they transform the raw input features in nonlinear ways and then perform linear regression on this transformed (latent) representation. This is very similar to using basis functions (i.e. polynomial features) in linear regression, except that in the case of NNs, the basis functions are learned from data. We also saw, that using regression is absolutely central when working with overparameterized models such as NNs. Now we can revisit our polynomial regression example from chapter 2 with this new knowledge. Let's look at the data again:

```{figure} notebooks/kernels_reg_data_1d_nonlin_train_val.png
:alt: "1d data"
:width: 400px
:align: center

Non-linear 1D data.
```

As you remember, using higher-order polynomials on this data caused severe overfitting. Using a validation set, we found the optimal degree of polynomials to be around 7 for this data. Limiting the degree of the polynomial is actually already a form of regularization, though we didn't use that term at the time. What if we alternatively use $\mathcal{l}_{2}$ regularization? This leads to a penalized loss function:

$$
\mathcal{L} = \sum_{i=1}^{N_\mathrm{train}} (f(x_i)-y_i)^2 + \lambda \sum_{j=1}^{N_\mathrm{feat}} w_j^2,
$$

where $\lambda$ is a regularization parameter. Here we adapt the notation from chapter 2 slightly to be consistent with the chapter on neural networks, by calling the regression coefficients *weights* $w_j$. In a linear model with polynomial basis functions: 

$$
f(x_i)=\sum_{j=1}^{N_\mathrm{feat}} w_j \phi_j(x_i),
$$ 

where $\phi_j(x_i)$ is a basis function transform such as $x_i^2$. For convenience, we typically precompute the transformed features and store them in the *design matrix* $\mathbf{X}$ with elements

$$
X_{ij} = \phi_j(x_i)
$$

Each collumn of this matrix is called a *feature vector* $\mathbf{x_i}$ and can be understood as the representation of a given datapoint $i$. With this, the loss function can be rewritten as:

$$
\mathcal{L} = || \mathbf{X}\mathbf{w} - \mathbf{y} ||^2 + \lambda ||\mathbf{w}||^2,
$$

where $\mathbf{w}$ is the vector of regression weights and $\mathbf{y}$ is the vector of training labels. As for conventional linear regression, we can find the optimal weights by setting the derivative of $\mathcal{L}$ with respect $\mathbf{w}$ to zero and solving for $\mathbf{w}$. This leads to the expression:

$$
\mathbf{w} = (\mathbf{X}^T\mathbf{X} - \lambda \mathbf{1})^{-1}\mathbf{X}^T\mathbf{y} 
$$

This means that we again get a closed-form expression for the weights. In fact, this solution even works when the number of parameters $N_\mathrm{feat}$ is larger than the number of training examples $N_\mathrm{train}$, because the regularization term provides an additional constraint to the solution. So, while there are infinitely many tenth-order polynomials that you can fit to five datapoints, there is only a single optimal solution when you add the regularizer.

This form of regularized regression is known as *Ridge Regression*. In this context, $\mathcal{l}_{2}$ regularization is also known as Tikhonov regularization. We can now apply this to our non-linear dataset from before. However, instead of limiting the degree of our fitting polynomial to avoid overfitting, we will now increase the magnitude of the regularization parameter $\lambda$ for a 14th degree polynomial:

```{figure} notebooks/kernels_ridge_polynomial.png
:alt: "1d data"
:width: 400px
:align: center

Fitting an unregularized and a regularized 14th-order polynomial to the non-linear data. $\lambda$ is a hyperparameter which tunes the regularization strength.
```

This shows that the unregularized polynomial is a horrible overfit, as expected. A reasonable regularization parameter of 0.1 leads to a very nice fit, however. If we would crank up the regularization to unreasonably large sizes, we end up with an underfitted function, that is strongly biased towards zero. This, of course, raises the question of what a reasonable size for $\lambda$ is. To this end, we can compare the training and validation errors:

```{figure} notebooks/kernels_rdige_train_val_loss.png
:alt: "validation and training errors for ridge regression"
:width: 400px
:align: center

Validation and training set errors for ridge regression (using the 14th-order polynomial from above). $\lambda$ is a hyperparameter which tunes the regularization strength.
```

This shows that there is a quite large plateau of values that lead to essentially constant validation set errors, while the training error continuously increases as we increase the regularization strength. This is very common, so that it is often not necessary to optimize the regularizer very precisely (orders of magnitude count).

## The Dual Form of Linear Regression

If regularization allows us to build highly flexible polynomial fits, you might wonder why we should bother with ML at all. This is a fair question. Regularized linear models are actually very useful and often an excellent choice. However, there are also some practical limitations, in particular when using a polynomial basis in higher dimensions. For example, for a two dimensional input the second degree polynomial, feature vectors $\mathbf{x_i}$ take the form:

$$
\mathbf{x_i} = [1,x_{i1},x_{i2},x_{i1}x_{i2},x_{i1}^2,x_{i2}^2]
$$

In addition to raising features to higher powers, we thus also obtain *interaction terms*. For high dimensional input data and high order polynomials this quickly leads to an enormous number of features. Specifically, the number of features generated in this manner scales exponentially with the degree of the polynomial and polynomially with the number of input dimensions:

```{figure} notebooks/kernels_ridge_number_of_features.png
:alt: "validation and training errors for ridge regression"
:width: 400px
:align: center

Numbers of features for polynomial basis functions, given the dimensionality of the input features and order of the polynomial. 
```

Since ML methods are often applied to high dimensional data, this is a problem for us. Fortunately, there is a neat trick which we can use to avoid this explosion of features. To this end, we reformulate the linear regression problem in the so-called *dual form* (in contrast to the *primal form* we have been using so far).

The key idea behind the dual form is the *representer theorem*, which states that for the kind of loss function we are considering, the optimal weights can be expressed as:

$$
w_j = \sum_i^{N_\mathrm{train}} \alpha_i X_{i,j}.
$$

Here we introduced a new set of regression weights $\alpha_i$, sometimes called the *dual coefficients*. In words, this means that the *primal* regression weights can be expressed in terms of the *dual* weights and the features of the dataset. Importantly, there are $N_\mathrm{feat}$ primal weights $w_{j}$ and $N_\mathrm{train}$ dual weights $\alpha_i$.

Using vectors and matrices, this becomes:

$$
\mathbf{w} = \mathbf{X}^T \boldsymbol{\alpha},
$$

which we can substitute into the loss function:

$$
\mathcal{L} = || \mathbf{X}\mathbf{X}^T \boldsymbol{\alpha} - \mathbf{y} ||^2 + \lambda ||\mathbf{X}^T \boldsymbol{\alpha}||^2,
$$

As before, we set the derivative of $\mathcal{L}$ with respect $\boldsymbol{\alpha}$ to zero and solve for $\boldsymbol{\alpha}$. This leads to the expression:

$$
\boldsymbol{\alpha} = (\mathbf{X}^T\mathbf{X} - \lambda \mathbf{1})^{-1}\mathbf{y} 
$$

This looks similar to the expression for the primal coefficients, but the design matrix $\mathbf{X}$ now only appears in the term $\mathbf{X}^T\mathbf{X}$. This matrix product is extremely important to us, so we will give it its own symbol $\mathbf{K}$. $\mathbf{K}$ is called the *Kernel matrix* (alternatively: covariance matrix).

At this point, it should be noted that the primal and dual forms of ridge regression yield exactly the same results. The only difference is the number of parameters that we are optimizing. This means that the dual form has significant practical advantages when $N_\mathrm{feat} >> N_\mathrm{train}$. In all other respects, it is still the same method, however. To go beyond conventional ridge regression, we need to learn more about Kernels.

## Kernels

The Kernel matrix $\mathbf{K}=\mathbf{X}^T\mathbf{X}$ we uses for obtaining the dual regression weights has dimensions $N_\mathrm{train} \times N_\mathrm{train}$. Each entry in this (symmetric) matrix thus corresponds to a pair of training datapoints $(ij)$. More precisely:

$$
K_{ij} = \sum_{k=1}^{N_\mathrm{feat}} X_{ik} X_{jk} = \mathbf{x}_i \cdot \mathbf{x}_j
$$

In words, the Kernel matrix is built from scalar (or inner) products of feature vectors. We can analogously define the linear Kernel function as:

$$
K(\mathbf{x}_i,\mathbf{x}_j) = \mathbf{x}_i \cdot \mathbf{x}_j
$$

So far, we only talked about fitting linear models in the dual form, but of course we can use them for prediction on unknown data as well. For a single datapoint with the feature vector $\mathbf{x}_u$, this reads:

$$
f(\mathbf{x}_u) = \mathbf{x}_u \mathbf{w} = \mathbf{x}_u \mathbf{X}^T \boldsymbol{\alpha}
$$

We can evaluate this using our Kernel function as:

$$
f(\mathbf{x}_u) = \sum_i^{N_\mathrm{train}} \alpha_{i} K(\mathbf{x}_i,\mathbf{x}_u)
$$

This means that the value at the target point $\mathbf{x}_u$ is given by a linear combination of Kernel functions between $\mathbf{x}_u$ and the training datapoints $\mathbf{x}_i$, weighted by the dual coefficients $\alpha_{i}$. This is a disadvantage of the dual form, because it means that we need to store our training set in memory (in addition to the regression weights) for predictions. It also means that function evaluation is more expensive (particularly for large datasets) in the dual form than in the primal form. 


## Kernel Ridge Regression 

You may have noticed that I used the term *linear* Kernel function above, which implies that there are also other Kernels. Indeed, we can use a large variety of other kernel functions as well, as long as they abide by some simple rules: In particular, the Kernel matrix must be *positive semidefinite* and the Kernel function must be symmetric to an exchange of the arguments. There is a large number of functions that fulfill these properties, the simplest of these being the *polynomial Kernel* of degree $d$:

$$
K(\mathbf{x}_i,\mathbf{x}_j) = (\mathbf{x}_i \cdot \mathbf{x}_j+c)^d =  (\sum_{k=1}^{N_\mathrm{feat}} x_{ik} x_{jk}+c)^d,
$$

where $c \geq 0$ is a parameter (often set to 0 or 1). Lets consider a specific case where $N_\mathrm{feat}=2$, $c=1$ and $d=2$:

$$
K(\mathbf{x}_i,\mathbf{x}_j) = ( x_{i1} x_{j1} + x_{i2} x_{j2} + 1)^2 = x_{i1}^2 x_{j1}^2 + x_{i2}^2 x_{j2}^2 + 2 (x_{i1} x_{i2}) (x_{j1} x_{j2}) + x_{i1} x_{j1} + x_{i2} x_{j2} + 1
$$

The key insight behind this equation is that this polynomial kernel expression is exactly equivalent to the scalar product (and thus to the linear kernel) of the following modified features:

$$
\mathbf{\tilde{x}_i} = [1,\sqrt{2}x_{i1},\sqrt{2}x_{i2},\sqrt{2}x_{i1}x_{i2},x_{i1}^2,x_{i2}^2]
$$

These look suspiciously like the polynomial features we defined above, except for small differences in the prefactors which can be absorbed by the regression coefficients. This means that ridge regression with the polynomial kernel and the raw features is basically equivalent to ridge regression with the linear kernel and polynomial features. The neat thing about the method we just defined (called *Kernel Ridge Regression*, KRR) is that we never need to actually calculate all those polynomial features because all we need is the Kernel matrix. To calculate this, we just use the kernel function with the raw input features. This is a huge advantage, given how the number of polynomial features can explode in high dimensional problems. In KRR, the memory requirements and computational effort is exactly the same, no matter how high the order of the polynomial kernel is. This is called the *kernel trick*.

Because we never actually need the basis functions in KRR, we can also define Kernel functions that do not even have a explicit set of features associated with them. An example of this is the Radial Basis Function (RBF) kernel (also known as the Gaussian or Squared Exponential Kernel):

$$
K(\mathbf{x}_i,\mathbf{x}_j) = \exp\left(-\gamma||\mathbf{x}_i-\mathbf{x}_j||^2\right),
$$

where $\gamma$ is a hyperparameter that defines the lengthscale of the Kernel (more on this below). Interestingly, this Kernel also defines a scalar product, but between infinitely large feature vectors (since the exponential function can be written as an infinite Taylor series). In practice, we don't have to worry about this feature space, though.

I've skimmed over some of the mathematical details here (you may for example read about *Reproducing Kernel Hilbert Spaces* in the literature). For the purposes of this course, it is enough to know that KRR with the RBF Kernel is a powerful ML method. It combines the functional flexibility of an infinitely dimensional feature space with the simplicity of fitting a linear model. Compared to NNs, the number of hyperparameters is also significantly smaller: We merely need to select a regularization strength $\lambda$ and the Kernel lengthscale $\gamma$. For the nonlinear dataset above, this looks like:

```{figure} notebooks/kernels_krr_rbf.png
:alt: "rbf fit to nonlinear data"
:width: 400px
:align: center

Fitting a KRR model with the RBF Kernel to nonlinear data. $\gamma$ is a hyperparameter which governs the lengthscale of the Kernel.
```

## Kernels as Similarity Measures

So far, I have introduced KRR as a generalization of regularized linear regression, which takes advantage of the dual formulation to efficiently work with large or infinite feature spaces. This is mathematically rigorous, but not particularly intuitive. A different way of understanding KRR is in terms of similarity measures. Indeed, the RBF Kernel is just that: If $\mathbf{x}_i=\mathbf{x}_j$ then $||\mathbf{x}_i-\mathbf{x}_j||=0$ and $K(\mathbf{x}_i,\mathbf{x}_j)=1$. In contrast, if $||\mathbf{x}_i-\mathbf{x}_j||>>0$ then $K(\mathbf{x}_i,\mathbf{x}_j)=0$. In other words for identical input vectors the similarity is one, for very different input vectors it is zero, and it varies smoothly in between. You may remember this picture, illustrating the Kernel ML with cat and dog pictures.

```{figure} notebooks/Cat_Dog_Kernel_Matrix.png
:alt: "A Kernel Matrix comparing cats and dogs"
:width: 400px
:align: center

A ficticious Kernel matrix, quantifying the similarity between cats and dogs on a scale from zero to one.
```

For the linear Kernel, this similarity measure might be less intuitive. Taking a geometric analogy, the scalar product is large when two vectors are pointing in the same direction and zero when they are orthogonal. Unlike the RBF Kernel, this measure is not between zero and one, however. Therefore, it is common to normalize the linear and polynomial kernels. In general, a normalized Kernel is defined as:

$$
\tilde{K}(A,B) = \frac{K(A,B)}{\sqrt{K(A,A)K(B,B)}}
$$

Based on this idea of similarity, we can understand KRR in a more intuitive way: The value of a function at an unknown target point $\mathbf{x}_u$ is estimated by taking a weighted average of coefficients $\alpha_{i}$ (one for each training point $\mathbf{x}_i$), where the weight of each coefficients is given by the similarity between $\mathbf{x}_i$ and $\mathbf{x}_u$. In other words: We estimate the value of the unknown point by using information about the most similar points in our training set.

### Digression: k-Nearest-Neighbors

If we take the idea of using similarity to build a ML model, we get the k-Nearest-Neighbors (kNN) method, which is perhaps the simplest possible ML method. In kNN, we simply select the $k$ nearest neighbors (where $k$ is a hyperparameter) to an unknown data point from our training set and average their value:

$$
f(\mathbf{x}_u) = \frac{1}{k} \sum_{i \in NN} y_{i}
$$

```{figure} notebooks/kernels_knn.png
:alt: "k-nearest-neighbors"
:width: 400px
:align: center

Fitting non-linear data with the k-nearest-neighbors method.
```

This actually works reasonably well, but it has an obvious downside: The resulting function is not smooth. In this sense, KRR can be understood as a smooth version of the kNN method. 


## Kernel Regression for TinyFold

Now that we understand the basic idea behind Kernel ML, let's apply it to a more chemical dataset: The TinyFold folding energies from the previous chapters. If we use the same representation as for the NN models (a padded one-hot encoding of the atom types), setting up a KRR model is quite simple: Essentially we only need to select a Kernel function and the corresponding hyperparameters, which we can select based on a validation set. We consider three examples in the following, namely the linear, polynomial and RBF kernels.

For the polynomial kernel we only need to choose the regularization strength $\lambda$ and the degree of the polynomial $d$ (for simplicity we set the hyperparameter $c$ to zero throughout). The validation set MAE as a function of these hyperparameters is shown below:

```{figure} notebooks/kernels_krr_poly_tinyfold.png
:alt: "KRR polynomials TinyFold"
:width: 400px
:align: center

Validation set errors for folding energy predictions, using KRR with the polynomial Kernel, as a function of degree and regularization strength $\lambda$.
```

This shows that KRR is a big improvement over conventional linear regression, which corresponds to degree 1. We also see that the thrid order kernel is still a significant improvement over the second order one, while degree 4 doesn't bring much benefit (and is more sensitive to the regularizer). Overall, this approach yields a validation MAE of around 0.18, for the best Kernel. Importantly, there is no practical difference in terms of computational cost between these models, thanks to the Kernel trick. In normal linear regression, the fourth order polynomial features would be much more expensive to calculate.

Moving to the RBF Kernel, there are also two hyperparameters to consider: The regularization strength $\lambda$ and the lengthscale of the Kernel, $\gamma$. Since both of these are continuous variables, it is more convenient to visualize this dependence as a heat map:

```{figure} notebooks/kernels_krr_rbf_tinyfold_scan.png
:alt: "KRR RBF TinyFold"
:width: 400px
:align: center

Validation set errors for folding energy predictions, using KRR with the RBF Kernel, as a function of kernel lengthscale $\gamma$ and regularization strength $\lambda$.
```

This reveals a commonly observed feature, namely that there is a 'valley' of nearly optimal values in this plot. This is because both large values of $\lambda$ and small values of $\gamma$ lead to smoother (more regular functions), so that there is a trade-off between these two parameters. Interestingly, the best validation MAE obtain in this scan is actually almost identical to the third order polynomial kernel (around 0.18).

At this point we have explored the available hyperparameters with the validation set and found that both the RBF and polynomial kernels yield reasonably good resuls. We can therefore choose the third order polynomial model with optimized regularization as our 'final' model and apply it to the test set:

```{figure} notebooks/kernels_krr_test_set_correlation_plot.png
:alt: "KRR poly TinyFold test set correlation plot"
:width: 400px
:align: center

Correlation plot of test set predictions using a KRR model with a polynomial Kernel (third order), trained on the full set of 3000 training examples.
```

This shows that our model did learn a good predictive model for the folding energies, with a test MAE of 0.17. However, the NN model from the previous chapter performed significantly better (test MAE=0.1). If we want to further improve our Kernel model, we do have one more option, however: We can change the representation.

## N-Grams and Bags of Words

Indeed, the one-hot representation we have been using so far is perhaps overly simple. How can we do better? Since our protein sequences are basically strings ('AAA','ABABAA', etc.), we can turn to the world of natural language processing (NLP) for inspiration. A common representation for documents and strings is the *Bag of Words*, which essentially just counts the occurence of words in a given text (or, in our case, characters in a string). A more sophisticated version of this uses *n-grams* instead of words. Simply put, *n-grams* are string fragments of up to $n$ words or characters. For example, the string 'AABA' consists of: 

1) Uni-grams 'A' (3 times) and 'B' (1 time)
2) Bi-grams 'AA', 'AB' and 'BA' (each 1 time)
3) Tri-grams 'AAB' and 'ABA' (each 1 time)

By providing counts of these n-grams, the Bag of Words represents a string in terms of its components and how they are related to each other. In this way, it is related to the Many-Body-Tensor Representation we discussed previously. In this particular example, the 'Bag of n-grams' for 'AABA' would be `[3 1 1 1 1 1 1 1]`. 

If we use the n-gram representation with uni-, bi- and tri-grams we indeed find significant improvements in terms of the predictive performance, particularly for the linear kernel:

```{figure} notebooks/kernels_krr_tinyfold_models_reps.png
:alt: "KRR TinyFold test set model comparison"
:width: 400px
:align: center

Test errors of folding energy prediction models using linear and (third-order) polynomial Kernels and either the one-hot or n-gram representation. The latter performs significantly better.
```

This reemphasizes a point we have made previously. The non-linearity of ML models can compensate for weaknesses in the representation to an extent. Nonetheless, a better representation also helps ML models, particularly in terms of making them more robust. It is therefore a good idea to use simple models (e.g. the linear Kernel) to compare representations.

## Data-Efficiency

So far, all of these results were obtained for the full training set of 3000 structures. How does the test error change when we consider smaller set? This can be nicely visualized in a learning curve:

```{figure} notebooks/kernels_learning_curve_test_vs_ntrain.png
:alt: "KRR and NN TinyFold test set learning curves"
:width: 400px
:align: center

Test errors of folding energy prediction models for KRR (third-order polynomial Kernel and n-gram representation) and the NN models from the previous chapter. Note that the KRR model ouperforms the NNs for small training sets, but eventually saturates
```

This is a quite typical result: For the smallest training sets, KRR is significantly better than the NN, but the Kernel models stop improving as much for larger datasets. Here the NNs benefit from their greater flexibility. This reinforces the notion that Kernel models are useful for data-efficient ML.


## Kernel Methods Beyond Regression

Kernel models are a large and flexible family, including Support Vector Machines, Gaussian Process Regression, Kernel Density Estimators, Kernel Principal Component Analysis and many more. In principle, any linear model can be *kernelized*. So far we have focused on regression, but there are also Kernel models for classification, clustering and dimensionality reduction. As a brief example of the latter, we can consider the Kernel Principal Component Analysis (KPCA) method, which allows visualizing high dimensional data, the way it is percieved by the Kernel. For the tinyfold data with the one-hot encoding this looks like this:

```{figure} notebooks/kernels_kpca_onehot.png
:alt: "KPCA Tinyfold one-hot"
:width: 400px
:align: center

KPCA visualization of the TinyFold training set using the one-hot representation. Each point corresponds to one sequence, colored by its folding energy. A third-order polynomial Kernel is used.
```

And with the n-gram representation:

```{figure} notebooks/kernels_kpca_ngram.png
:alt: "KPCA Tinyfold one-hot"
:width: 400px
:align: center

Same as above, but using the n-gram representation.
```

In these plots, each point corresponds to one sequence, colored by its folding energy. Points that are close to each other in the plot have a high Kernel similarity. From this, we can readily see why the n-gram representation is more powerful than the one-hot encoding: It naturally separates systems with high folding energy form ones with a low energy. It is thus easier to fit a regression model in this space.