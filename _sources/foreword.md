# Chemical Machine Learning

It is impossible to avoid the current hype around Machine Learning (ML), Artificial Intelligence (AI) and Data Science. Chemistry is no exception to this, with claims of 'revolutionary' AI developments (in drug discovery, material design, etc.) appearing almost on a daily basis. In many cases, these things turn out to be somewhat less groundbreaking than promised (it is a hype after all). But there is no doubt that in many areas (*i.e.* protein structure prediction or forcefield development) remarkable scientific progress has been achieved thanks to ML. In short: ML and data science are here to stay. Future generations of chemists should therefore at least be literate in this field, both to take advantage of its benefits and to distinguish the empty hype from the 'real deal'.

With this goal in mind, this course will cover some essential topics in *Chemical Machine Learning*. The target audience are all chemists interested in digitalization and data science. We will address a wide range of applications from theoretical chemistry and molecular design to synthesis planning and process optimization. 

The accompanying tutorial will allow an interactive exploration of the examples discussed in the lecture. Programming knowledge is not a prerequisite, but you should be open to learn. Similarly, we will not cover any mathematical proofs in the lecture, but maths are an important part of ML. Without following the equations, it will be very difficult to truly understand some of the key concepts.

The course is roughly structured into three parts: the first 6 lectures will be on the fundamentals of chemical ML. The next 3 will cover prototypical applications: prediction of molecular properties, classification of molecules and generation (discovery) of new molecules. The final 3 lectures will cover some advanced concepts, to illustrate what cutting edge research in chemical ML looks like.

Finally, an obligatory warning: ML is a very broad and quickly developing field. I try to cover a lot of ground here, in order to hopefully spark your interest and inspire you to dig deeper. Nonetheless, there are certainly many important aspects that are missing and many cases where we only scratch the surface. To mitigate this, I provide some further reading suggestions at the end of each chapter.

```{warning} 
This script is in constant development. It's worth checking in frequently!
```

Johannes Margraf ([contact me!](mailto:margraf@fhi.mpg.de))

Berlin, December 2021