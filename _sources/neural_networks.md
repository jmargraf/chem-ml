# Neural Networks

Artificial neural networks (or simply Neural Networks, NNs) are one of the dominant approaches in machine learning. They are inspired by how biological neural networks (*i.e.* animal brains) process signals. The concept of computing algorithms based on NNs is actually quite old: It was first proposed in the 1940s, with first implementations being performed in the 1960s and 1970s. Despite this long tradition, NNs were mostly of academic interest for a long time. In "real world" applications, deterministic expert systems and kernel based ML methods (*e.g.* Support Vector Machines) were more common, mostly because they were easier to train and more robust. This has changed dramatically since the 2010s, when NN-based models started to outperform other methods, sometimes by large margins. This is thanks to a combination of software and hardware advances, which allow training deep neural networks efficiently. 

## Basics

While NNs are inspired by how animal brains work, this should mostly be understood as an inspiration or an analogy. In most cases, NNs are not intended to provide a faithful picture of how natural neural networks operate. Instead, NNs are used as extremely flexible fitting functions. The most common (and simple) kind of NN is a feed-forward neural network, also known as a multi-layer perceptron (MLP). Such a network is shown below:

```{figure} notebooks/Neural_Network_Sketch.png
:alt: "Sketch of a Feed-Forward Neural Network"
:width: 400px
:align: center

Sketch of a feed-forward neural network. The input layer (I) consists of four neurons, followed by four hidden layers of 8 neurons each (H) and an output layer consisting of a single neuron. Note that the neurons are exclusively connected to other neurons in the previous and subsequent layers. The colours map to numerical values from -1 to 1. Each neuron has a *state*, and each connection has a *weight*. Training a neural network simply means adjusting the weights to obtain the desired output.
```

The basic idea is that the data flows through the network so that the raw input features are succesively transformed and finally converted to the output. Just as with linear models, the input and output can in principle have arbitrary shapes. Indeed, we can generally think about neural networks as an extension of linear models. 

### Linear Transformations

As you remember, linear transformations can be written as matrix-vector multiplications:

$$
\mathbf{W} \mathbf{x} = \mathbf{y}
$$

Here, the input vector $\mathbf{x}$ (with dimension $N_\mathrm{in}$) is multiplied with a *weight matrix* $\mathbf{W}$ (with shape $N_\mathrm{out} \times N_\mathrm{in}$) to form a new vector $\mathbf{y}$ (with dimension $N_\mathrm{out}$). This transformation can be further expanded with a *bias vector* $\mathbf{b}$ (with dimension $N_\mathrm{out}$):

$$
\mathbf{W} \mathbf{x} + \mathbf{b} = \mathbf{y}
$$

The weights and biases in $\mathbf{W}$ and $\mathbf{b}$ are the learnable parameters of neural networks. In principle, we could use a chain of linear transformation to build a NN architecture. For simplicity, let's only consider *weight matrices* for now. In this case a 'deep linear model' could be defined by the following sequence of operations:

$$
\mathbf{W_{1,in}} \mathbf{x} = \mathbf{h_{1}}
$$
$$
\mathbf{W_{2,1}} \mathbf{h_{1}} = \mathbf{h_{2}}
$$
$$
\mathbf{W_{3,2}} \mathbf{h_{2}} = \mathbf{h_{3}}
$$
$$
\mathbf{W_{out,3}} \mathbf{h_{3}} = \mathbf{y}
$$

Here, the vectors $\mathbf{h_i}$ correspond to the hidden states of the network. In a single equation this can be combined to:

$$
\mathbf{W_{out,3}}(\mathbf{W_{3,2}}(\mathbf{W_{2,1}}(\mathbf{W_{1,in}} \mathbf{x}))) = \mathbf{y}
$$

As you can see, the vectors $\mathbf{h_i}$ do not appear in this equation, which explains why we call them hidden. At first glance, this looks like some kind of expanded linear model with additional parameters. Unfortunately, such a 'deep linear model' do not actually make sense. This is because the weight matrices can all be multiplied, to yield a single weight matrix:

$$
\mathbf{W_{3,out}} \mathbf{W_{2,3}} \mathbf{W_{1,2}} \mathbf{W_{in,1}} = \mathbf{W_{out,in}} 
$$

So the deep linear model is actually equivalent to a normal linear model with

$$
\mathbf{W_{out,in}} \mathbf{x} = \mathbf{y}
$$

### Activation Functions

To profit from the additional flexibility of the weight matrices, we must therefore break the chain of matrix multiplications. In other words, we have to ensure that the weights are linearly independent from each other. In NNs, this is the role of the activation functions $\sigma$. This is a nonlinear function that is applied to the hidden state vectors element-wise: 

$$
\sigma(\mathbf{W_{1,in}} \mathbf{x}) = \mathbf{h_{1}}
$$
$$
\sigma(\mathbf{W_{2,1}} \mathbf{h_{1}}) = \mathbf{h_{2}}
$$
$$
\sigma(\mathbf{W_{3,2}} \mathbf{h_{2}}) = \mathbf{h_{3}}
$$
$$
\mathbf{W_{out,3}} \mathbf{h_{3}} = \mathbf{y}
$$

In a single equation, the output of our NN is thus:

$$
\mathbf{W_{out,3}}\sigma(\mathbf{W_{3,2}}\sigma(\mathbf{W_{2,1}}\sigma(\mathbf{W_{1,in}} \mathbf{x}))) = \mathbf{y}
$$

As you can see, $\sigma$ effectively separates the weight matrices from each other, so that the expression cannot be reduced to a single matrix multiplication. The job of the activation function is simply to introduce non-linearity to the network. As you can see, no activation function is applied to the last transformation. This underscores the relationship between linear regression and NNs. In fact, we could write the output of the NN as:

$$
\mathbf{W_{out,3}} \mathbf{x}_\mathrm{latent} = \mathbf{y}
$$

with the *latent representation* defined as

$$
\mathbf{x}_\mathrm{latent} = \sigma(\mathbf{W_{3,2}}\sigma(\mathbf{W_{2,1}}\sigma(\mathbf{W_{1,in}} \mathbf{x})))
$$

In this sense, the final layer (parameterized by $\mathbf{W_{out,3}}$) is identical to the coefficient matrix in a normal linear regression model, while the job of the other weight matrices is to transform the raw representation vector $\mathbf{x}$ into a better *latent* representation $\mathbf{x}_\mathrm{latent}$. NNs are consequently often understood as a tool for *representation learning*. This means that with NNs we need to worry less about basis functions and feature engineering, because the network is learning the ideal basis functions from the data. The vector space spanned by this learned representation is sometimes called *latent* or *feature space*.

To a first approximation, it doesn't even matter very much what kind of non-linearity is used, although in practice there are some factors to consider. One of the first widely used activation functions was the sigmoid (a.k.a. logistic) function:

$$
\sigma_\mathrm{sig}(x) = \frac{1}{1+e^{-x}}
$$

```{figure} notebooks/sigmoid.png
:alt: "Plot of the sigmoid activation function"
:width: 400px
:align: center

Plot of the sigmoid activation function.
```

This is a *saturating* activation function, as it converges to constant values of zero and one for large negative or positive inputs. This can lead to the so-called *vanishing gradients* problem when optimizing the parameters of the network (see below). This is especially problematic for deep neural networks, and has led to the development of *non-saturating* activation functions. The most famous of these is the *Rectified Linear Unit* (ReLU):

$$
\sigma_\mathrm{relu}(x) = \max(0,x)
$$

```{figure} notebooks/relu.png
:alt: "Plot of the ReLU activation function"
:width: 400px
:align: center

Plot of the ReLU activation function.
```

ReLU is perhaps the most trivial non-linearity imaginable: It simply sets all negative inputs to zero and is linear in the positive range. Nonetheless, it is very commonly used, as it avoids the vanishing gradient problem and leads to very accurate deep neural networks. This shows that the kind of non-linearity is indeed not very critical for neural networks. There are, of course, also some downsides to the simplicity of ReLU however. For one, it radically quenches all negative inputs, which means that it essentially removes some neurons from the network. Additionally, it also leads to non-smooth functions, which can be problematic, e.g. when building force fields. There are therefore a number of modifications of ReLU, which address these shortcomings. One example is the SiLU functions, which combines the sigmoid and ReLU:

$$
\sigma_\mathrm{SiLU}(x) = x \frac{1}{1+e^{-x}} = x \sigma_\mathrm{sig}(x)
$$

```{figure} notebooks/silu.png
:alt: "Plot of the SiLU activation function"
:width: 400px
:align: center

Plot of the SiLU activation function.
```

While the nature of the non-linearity is not crucially important for many applications, it does affect the properties of the final function. This can be seen for a simple example, where NNs with different activation functions are fitted to five datapoints of a one-dimensional function:

```{figure} notebooks/activations.png
:alt: "Fitting simple NNs to 1D data"
:width: 400px
:align: center

Plot of simple NN fits to five datapoints, using different activation functions.
```

You can see that all three fits perfectly pass through the datapoints. Nonetheless, the functions behave differently *between* the points. While the ReLU and SiLU NNs resemble piecewise linear functions, the sigmoid NN is smoother and tends to have an S-shaped character. A closer look also reveals that the SiLU function is smooth at turning points, while the ReLU function is not. Clearly, the choice of the activation function thus affects how the networks generalize. ReLU and SiLU based networks have a bias towards linearity, sigmoid networks have a bias towards switching behaviour. Consequently, the latter are more commonly employed for classification than for regression.

### Universal Approximation Theorems

The theoretical analysis of artificials neural network has led to several famous *Universal Approximation Theorems*. Basically, these state that any continuous function $f(\mathbf{x}) = \mathbf{y}$ can be perfectly represented by a NN, provided that is has either infinite *width* (one hidden layer with infinitely many neurons) or *depth* (an infinite number of hidden layers with a finite number of neurons each). This universal approximation character is the main appeal of NNs: They can be applied to any kind of data that can be fit by a continuous function, which explains the wide range of applications they are used for. In particular, they are extremely useful whenever we have no knowledge about the analytical form of the target function, or when the analytical form is known but intractable (Schrödinger equation!).

Of course, in practice we never use infinitely wide or deep networks. But by adding more neurons, we can in principle converge towards the target function with arbitrary accuracy. This raises the question of whether we should make our networks wider or deeper, in this case. Empirically, it seems that *deep neural networks* have an advantage, as they tend to learn features in a hierarchical way, which generalizes better. In contrast, shallow but wide networks may simply memorize the input data, which can lead to overfitting.

## Training

Having established what NNs are and why we might want to use them, the question is: How do we determine the optimal weights and biases? Unlike in linear regression, we cannot use a closed-form expression for the optimal parameters. This is the price we pay for the non-linearity introduced *via* the activation functions. Instead we will resort to variations of the gradient descent algorithm. As a reminder, the idea behind gradient descent is to take the derivatives of the loss function with respect to the parameters. In the case of neural networks, this is a little bit more complicated, however, because the weight matrices are chained together. Thankfully there is a standard procedure for obtaining these gradients, called *backpropagation*.

### Backpropagation

Consider a very simple NN with a single hidden layer:

```{figure} notebooks/Backprop_Image.png
:alt: "Simple NN to illustrate backpropagation"
:width: 400px
:align: center

Illustration of a simple neural network with a single hidden layer. A given input $\mathbf{x}$ is transformed to the hidden state $\mathbf{h}$, which is in turn transformed to the output $y$. The transformations are parameterized by the weight matrices $\mathbf{W}^{1}$ and $\mathbf{W^{2}}$.
```

Here we slightly adjusted the notation, in order to reduce the number of subscript indices. Mathematically, this network is defined by the equations:

$$
\sigma(\mathbf{W^{1}} \mathbf{x}) = \mathbf{h}
$$

and 

$$
\mathbf{W^{2}} \mathbf{h} = y
$$

For later use, we will furthermore define the weighted input of each layer as $\mathbf{z}^{l}$:

$$
\mathbf{z}^{1} = \mathbf{W^{1}} \mathbf{x} 
$$



$$
\mathbf{z}^{2} = \mathbf{W^{2}} \mathbf{h}
$$

For simplicity, let's imagine that we are fitting our weights to a single datapoint $y_\mathrm{ref}$. A squared loss for this problem is given by:

$$
\mathcal{L} = \frac{1}{2} (y-y_\mathrm{ref})^{2},
$$
where the factor $\frac{1}{2}$ is introduced for convenience. 

In order to perform gradient descent, we need the derivatives of $\mathcal{L}$ with respect to the weight matrices $\mathbf{W^{1}}$ and $\mathbf{W^{2}}$. 

In the backpropagation algorithm, these derivatives are obtained by computing an error vector $\delta^{l}$ for each layer in the network, defined as:

$$
\delta^{l} = \frac{\partial \mathcal{L}}{\partial \mathbf{z}^{l}}
$$

In words, this quantifies how the loss function reacts to changes in the input of a given layer. Loosely speaking, this assigns some responsibility for the total loss to each neuron in the network. If an element of $\delta^{l}$ is large, the loss can be decreased by changing the weights going into the corresponding neuron. 

We first need to calculate $\delta^{l}$ for each layer. To this end, we start from the output layer and work backwards. In our case this means, that we start with $\delta^{2}$. Applying the chain rule, we find that:

$$
\delta^{2} = \underbrace{(y-y_\mathrm{ref})}_{\frac{\partial \mathcal{L}}{\partial y}} * \underbrace{1}_{\frac{\partial y}{\partial \mathbf{z}^{l}}}
$$

It stands to reason that the error on the output layer is simply given by the loss function. We can then work backwards to the next layer, where $\delta^{1}$ can be computed as:

$$
\delta^{1} = (\mathbf{W^2}^{T} \delta^{2}) \odot \sigma'(\mathbf{z}^1).
$$

Here, $\odot$ indicates the element-wise multiplication of the two vectors or matrices and $\sigma'(\mathbf{z}^1)$ is the derivative of the activation function with respect to the input $\mathbf{z}^1$. Notably, this defines a recursive relationship between the error in the layer $l$ and $l+1$, which means that we can use these equations to compute the error on any layer for arbitrarily deep NNs.

Finally, the desired derivatives $\frac{\partial \mathcal{L}}{\partial \mathbf{W^{l}}}$ can be computed from the backpropagated errors according to:

$$
\frac{\partial \mathcal{L}}{\partial \mathbf{W^{2}}} = \mathbf{h} \otimes \delta^{2} 
$$

and 

$$
\frac{\partial \mathcal{L}}{\partial \mathbf{W^{1}}} = \mathbf{x} \otimes \delta^{1}
$$

Here, $\otimes$ denotes the outer product. In other words, the derivative for a specific weight $W^{1}_{ij}$ is given by:

$$
\frac{\partial \mathcal{L}}{\partial W^{1}_{ij}} = x_{i}\delta^{1}_{j}
$$

Thanks to the backpropagation trick, the derivative of each weight matrix can be calculated from a few simple components: The weighted inputs $\mathbf{z}^{l}$, the derivatives of the activation function $\sigma'$ and the outputs of each layer (in this case $\mathbf{x}$, $\mathbf{h}$ and $y$). Even more importantly: We don't actually have to do this by hand. An essential component of modern deep learning frameworks like PyTorch, TensorFlow or Jax is that they use *automatic differentiation*. This means that we only need to define our network in the forward direction, while the code for backpropagation is automatically generated.

Note that this has been a rather simplified and superficial introduction to backpropagation. A much more detailed discussion can be found [here](http://neuralnetworksanddeeplearning.com/chap2.html).

### Stochastic gradient descent

Having the derivatives in hand, we can now do gradient descent to optimize the weights. As discussed in the chapter on linear regression, this basically means that the weights at iteration $T+1$ are calculated as:

$$
\mathbf{W}_{T+1} = \mathbf{W}_{T} - \gamma \frac{\partial \mathcal{L_{T}}}{\partial \mathbf{W}},
$$
where $\gamma$ is the learning rate.

Since NNs are usually applied to big datasets, computing the loss function and its derivatives can become cumbersome. Furthermore, the learning rate $\gamma$ is usually quite small (e.g. 0.001), so that it takes many steps to converge. Consequently, NN weights are usually optimized with modified versions of gradient descent, based on the Stochastic Gradient Descent (SGD) approach.

In pure SGD, the training set is randomly ordered and a GD step is sequentially performed for each datapoint *individually*. In other words, each training sample is individually passed through the network, the corresponding gradients are calculated *via* backpropagation, and the weights are adjusted slightly. For a training set of 1000 samples, SGD thus does 1000 weight updates at each iteration, while full (a.k.a. *batch*) GD only does a single update. In practice, a compromise between these approaches is used, so called minibatch gradient descent. Here, the training set is divided into small batches (e.g. of 64 samples each), and each batch is passed through the network simultaneously. This is often optimal in terms of computational costs, because forward and backward passes can be computed very efficiently on GPUs for such batches.

### Full training loop

We have now defined all components necessary to train a neural network. These are:

1. *The forward pass:* Training samples are passed through the neural network in order to compute the loss.
2. *The backward pass:* The derivatives of the loss with respect to the trainable parameters are computed via backpropagation.
3. *The parameter update:* Using some form of gradient descent, the trainable parameters are updated.

If SGD or minibatch GD are used, these steps need to be repeated multiple times to iterate through the full training set. One such iteration is called an *epoch*. In practice, it is not uncommon to use hundreds or thousands of epochs for training. In pseudo-code the full training process thus reads:

```
while i_epoch < max_epochs:
    for minibatch in batches:
        inputs,targets = minibatch
        out = network(inputs)
        loss = mse(out,targets)
        gradients = backprop(loss)
        model = update(model,gradients,learning_rate)
```

## Digression: The TinyFold Dataset

We have now invested considerable time discussing how to train neural networks. There is a lot more to know about NNs, but at this point we can take a break and do a simple application, to get a feeling for how they work in practice. 

To this end, I created a little toy dataset called *TinyFold* inspired by the protein folding problem. Here, the proteins are represented by a two-dimensional polymer consisting of two amino acids called A and B. The interactions between these amino acids is such that there is a strong A-A attraction, a somewhat weaker B-B attraction and a strong A-B repulsion. Moreover, there is a angular term, which leads to an energetic penalty when the polymer bends. Taken together, these different interactions lead to a highly complex potential energy surface with many local minima and quite non-trivial folding behaviour.

```{figure} notebooks/tinyfold_collection.png
:alt: "Examples of local minima for toy peptide"
:width: 500px
:align: center

Examples of different local minima obtained when optimizing the geometry of a particular sequence in TinyFold (ABABBABAAABB). The global minimum is the top left structure.
```

The dataset itself consists of all possible sequences that can be generated with 3 to 12 amino acids (i.e. AAA, ABA, BAB, BBB, etc.). This leads to a total of 4216 sequences. For each of these, the (putative) global minimum structure was determined *via* extensive simulated annealing simulations. These global minima can be explored online with the [TinyFold app](http://www.tinyfold.xyz). Below the folding energies (*i.e.* the energy gained through intramolecular interactions in the folded state) of all sequences are shown.

```{figure} notebooks/tinyfold_energy_histogram.png
:alt: "Examples of local minima for toy peptide"
:width: 400px
:align: center

Histogram of folding energies for the global minima of all 4216 sequences in the dataset. Note that the energies are unit-less because this is a model system.
```

## Building a Regression Model for Folding Energies

We will now use everything we've learned so far to build a NN model that can predict the folding energy of a toy peptide based on the sequence. This is a non-trivial task, since the folding energy in principle can only be computed from the folded geometry, which requires an extensive global optimization. Predicting it directly from the sequence is thus quite a shortcut.

Before we get to the NN, we need to define a representation of the sequence (see the previous chapter). Since the amino acids are categorical, we will simply use a one-hot encoding here. An additional difficulty arises from the fact that the sequences have different lengths. To circumvent this, all sequences are extended to length 12 (the longest sequence in the database). This is achieves by adding dummy symbols to the end of each sting, which represent empty positions. We thus have three types of amino acids to encode ('A', 'B' and 'empty') and 12 positions, leading to a one-hot encoding of size $12 \times 3$.

```{figure} notebooks/tinyfold_one-hot.png
:alt: "one-hot encoding of toy protein"
:width: 600px
:align: center

Illustration of the one-hot encoding of a TinyFold protein sequence. To ensure that the representation has a fixed length, 'empty' positions are also encoded.
```

Next, we need to define the *architecture* of our neural network. It is clear that the input layer should have 36 neurons (the size of the representation) and the output layer should have one neuron (the folding energy). In between we will add two hidden layers with 64 neurons each. This choice is completely *ad hoc*, and propably not optimal. We will discuss more rational methods of network design in the next chapter.

Furthermore, we need to decide how the weights will be optimized. This means choosing the optimizer (we will use gradient descent with momentum), the learning rate (we use $\gamma=0.005$) and the batch size (64 in this case). Again, these choices are not optimal, and we'll talk about this more in later lectures. For now, these are reasonable defaults.

With this we could in principle start training our model. However, we have already seen that simply fitting a training set is not very difficult (especially with NNs). We will therefore split our data, in order to have an objective criterion for how good our model is. From now on we will typically use three datasets:

1. *Training set:* Used to optimize the trainable weights and biases of our NN.
2. *Validation set:* Used to check the quality of a model during training and to compare different architectures or training procedures.
3. *Test set:* Used to estimate the generalization error of the *final model*.

In the following we use 3000 configurations for training, 256 for validation and 960 for testing. Importantly, we randomize the ordering of the data before splitting, to avoid spurious correlations in the data (e.g. if they are ordered by size). Data splitting is always a trade-off, since we would in principle want to use as much training data as possible. At the same time, the validation and test sets need to be large enough to provide statistically meaningful estimates of the generalization error of the model. We will discuss some alternative techniques  in the next chapter.

Now we are ready to train our model. The training process is often visualized by plotting the training and validation loss against the number of training epochs (this is called a *learning curve*):

```{figure} notebooks/learning_curve.png
:alt: "learning curve of folding energy prediction"
:width: 400px
:align: center

Learning curve for folding energy prediction in the TinyFold dataset. As is often observed, the validation error saturates much earlier than the training error.
```

This curve shows that things are working reasonably well: Both training and validation errors decrease significantly, particularly over the first epochs. We also see that the validation error saturates after around 60 epochs, indicating that the model starts to overfit on the training data beyond this point. It is also notable that the loss oscillates somewhat, as a consequence of the stochastic optimization process. Nonetheless, things look quite good for our first attempt. For now, we will accept this as our final model, which means that we can check it's performance on the test set. We'll do this with a correlation plot, showing the reference folding energies of the test set against our predictions:

```{figure} notebooks/test_correlation_plot.png
:alt: "test set error of folding energy prediction"
:width: 400px
:align: center

Correlation plot for folding energy predictions on the test set. The mean absolute error (MAE) is also shown in the plot.
```

Great! Our predictions on the test set are well correlated with the reference values, with a mean absolute error of 0.11 (compared to a energy range of 14 units). Not bad for our first chemical ML model.