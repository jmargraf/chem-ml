# Representing Chemical Data

As we discussed in the introduction, the applications of ML range from computer vision to robotics and natural language processing. This implies that ML algorithms are in principle agnostic to the kind of data they are applied to. This is only true in theory, however. While a basic neural network can indeed be used for all of these tasks, there are usually lots of domain-specific features in sophisticated models. More importantly, before we even begin to train an ML model we must define a *representation* of our data. This representation is what is initially provided to the ML model. Most of the human intelligence required to build chemical ML models goes into defining a suitable representation for the task at hand. By spending a chapter talking about this, we will also gain more insight into the different kinds of settings where ML is applied in chemistry. 

Before we get into the details, I want to briefly draw a connection to the previous chapter. So far we talked about linear regression, which allows us to predict some output $\mathbf{y}$ from some input $\mathbf{x}$. The bold notation here implies that we may have multiple input and output variables, which are stored in a vector. The *representation* is simply a definition of what goes into the vectors $\mathbf{x}$ and $\mathbf{y}$. In the simplest case this is the raw data. As we will see, it usually makes sense to preprocess your data, however. This preprocessing step is sometimes called *featurization* and the terms *represenation* and *features* are often used interchangeably. I will use the term *feature* to refer to a single element of a multi-dimensional *representation*. In the polynomial fits of the previous chapter we were already using *polynomial features*, where the notation $x_{k,i}$ indicated the feature $k$ for data-point $i$. This notation is also used below.

There are three basic motivations for constructing a representation from your raw data:

1. **Machine Readability**: At the outset your raw data is often not processable by an ML algorithm. As a rule, ML models ultimately need numerical input. It is not obvious how you can translate a chemical stucture into a vector of numbers, however. Even if you have numerical data to work with, you may have missing values in your data table because an experiment failed. All of these questions need to be addressed by building a numerical representation before you can proceed to train your model.  

2. **Prior Information**: Sometimes we can build additional information about our research question into the representation. We will discuss this in detail when we get to representations of chemical structures, where symmetry and invariances are extremely useful. Another example might be that you can scale or normalize your data to ensure proper physical results.

3.  **Feature Engineering**: This is the extreme form of *featurization*. It basically means that you apply transformation of your raw features in order to improve the performance of an ML model. The use of polynomial basis functions in the previous chapter was a form of feature engineering.

These motivations are listed in order of importance. Clearly, machine readability is not optional. Without it, you have no ML model. Including prior information is usually a good idea, when it is available. Feature engineering can make a difference to the performance, but it is generally less important when you use sophisticated ML approaches like deep neural networks. Importantly, feature engineering can also lead to overfitting, so it should be handeled with care.

Having established why we need a representation of our data, let's discuss some concrete chemical examples.

## Physicochemical data

The most basic kind of chemical data is the one that results from physicochemical measurements. This includes simple operational data like reaction time and temperature, spectroscopic data like absorption wavelengths and chemical shifts and many other things we can measure like reaction yields, rate constants and excited state lifetimes. All of these quantities have in common that they are numerical and thus trivially machine-readable.

### A Simple Example

Consider the following data:

```{figure} notebooks/rep_arre.png
:alt: "Data 1D"
:width: 400px
:align: center

Plot of observed reaction rates versus temperature. The dotted line is a first-order polynomial (linear) fit. The dashed line is a third-order polynomial fit.
```

Here some (made-up) observed reaction rates are plotted against the reaction temperature. Both temperature and rate constant are numerical, so we can directly fit a regression model that allows predicting the rate for an unknown temperature, i.e. using the temperature $T$ as the input $x$ of our model, and the rate $k$ as the output $y$. As can be seen from the dotted line in the plot, this is not a great fit for the data, because the relationship between temperature and rate is non-linear.

At this point we can engage in some feature engineering by using polynomial basis functions. For example, instead of just using $x$ as the input, we can use $\{x,x^2,x^3\}$. This indeed leads to a much better interpolation (the dashed line in the figure). Feature engineering is not necessarily the first thing to try, however. One reason for this is that this can lead to overfitting, as we discussed in the previous chapter. We can control this to some extend using a validation set, but it still leads to problems when we move beyond the range of our data. Let's look at what our fitted models do when we extrapolate to lower temperatures:


```{figure} notebooks/rep_arre_extrap.png
:alt: "Data 1D"
:width: 400px
:align: center

Plot of observed reaction rates versus temperature, extrapolated to lower temperatures. The dotted line is a first-order polynomial (linear) fit. The dashed line is a third-order polynomial fit.
```

The linear and polynomial fits give completely opposite predictions of what will happen when we decrease the temperature, despite being fitted to the same data. In the former case, the rate keeps decreasing. In fact, the linear fit actually predicts negative rates for low temperatures, which is completely unphysical. On the other hand, the polynomial fit predicts that the rates will begin to rise again, when we decrease the temperature. This is also unreasonable, and the data itself gives no indication of this. This is simply an artifact of our polynomial fit.

There is a general lesson here about *interpolation* vs. *extrapolation*. It is often said that regression models and ML in particular should only be used for *interpolation*. This means that you should only apply your model in the range defined by the training data. I think that this statement is too strong. In high-dimensional data we are always extrapolating to some extent. Nonetheless, we have to check the range of validity of your model, ideally by testing it independently. It is, of course, also very important to use human intelligence when looking at ML predictions. The other lesson is that you should keep your model as simple as possible. This is why feature engineering should not be the first thing to try when constructing ML models. Instead, for this particular example, prior knowledge can save the day. 

### Scale

If you paid attention in your physical chemistry class, you probably know what comes next. The non-linear relationship between $T$ and $k$ can be explained by the Arrhenius equation:

$$
k = A\exp\left(\frac{-E_{a}}{k_{B}T}\right),
$$
where $A$ is a preexponential factor, $E_{a}$ is the activation energy and $k_{B}$ is the Boltzmann constant. Taking the logarithm we obtain:

$$
\log(k) = \log(A) - \frac{E_{a}}{k_{B}} \frac{1}{T}
$$

This means that there should be a linear relationship between $\log(k)$ and $\frac{1}{T}$ and indeed that is the case:

```{figure} notebooks/rep_arre_scaled.png
:alt: "Data 1D"
:width: 400px
:align: center

Plot of observed reaction rates versus temperature with adjusted scales. The dotted line is a first-order polynomial (i.e. linear) fit.
```

Looking at the data on this transformed scale makes it clear that a simple linear fit works much better here, as expected. But there is another nice feature of working on the log-scale, namely that the rate constant now cannot become negative. Instead, it just becomes smaller as the temperature goes down, approaching zero. This is much more reasonable than the previous two fits, and consequently the extrapolative behaviour of our fit is much better:

```{figure} notebooks/rep_arre_extrap_all.png
:alt: "Data 1D"
:width: 400px
:align: center

Plot of observed reaction rates versus temperature, extrapolated to lower temperatures. The dotted line is a first-order polynomial (linear) fit. The dashed line is a third-order polynomial fit. The solid line is the linear fit performed on the 1/T vs. log(k) scale.
```

This is an example of using prior knowledge to choose the representation of the data. Thanks to the physical insight from the Arrhenius equation we can transform the input data to make it easier to fit and more robust for extrapolation. Representation matters! 


### Standartization

Another common transformation of raw input data is standartization. To do this we first need to calculate the mean $\mu_{k}$ and standard deviation $\sigma_{k}$ of the raw input data:

$$
\mu_{k} = \frac{1}{N} \sum_{i}^{N} x_{i,k}
$$

$$
\sigma_{k} = \sqrt{ \frac{1}{N} \sum_{i}^{N} (x_{i,k}-\mu_{k})^2}
$$

Then we transform the inputs according to:

$$
\bar{x}_{i,k} = \frac{x_{i,k}-\mu_{k}}{\sigma_{k}}
$$

This means that the mean value of each variable in the dataset is now zero and the standard deviation is one. In other words: Your inputs will approximately lie between -1 and 1.

Standartization is very common when working with neural networks, which can be very sensitive to the range of the input data. However, it can also make sense when working with diverse types of input data. For example, we might have a dataset which captures the relation between some properties of a molecular photocatalyst and reaction yield:

| Catalyst    | Oxidation Potential | Oxidation State  | Absorption Wavelength | Yield  |
|-------------|---------------------|------------------|-----------------------|--------|
|    A        |       3.4 V         |        3         |          430 nm       |  50%   |      
|    B        |       5.2 V         |        2         |          330 nm       |  80%   |      
|    C        |       1.4 V         |        2         |          350 nm       |  60%   |      
|    D        |       2.4 V         |        3         |          440 nm       |  20%   |
|-------------|---------------------|------------------|-----------------------|--------|
|$\mu_{k}$    |       3.1 V         |       2.5        |          397.7 nm     |  51.5% |
|$\sigma_{k}$ |       1.4 V         |       0.5        |          48.1  nm     |  21.7% |

You can see that the different variables have quite different scales. Oxidation potentials range from 1.4-5.2V, absorption wavelengths from 330 nm to 440 nm. In this case standartization  makes sure that each variable has the same magnitude and variation to begin with. This is benefitial for the optimization of the parameters of your ML model and it makes them more interpretable. This is particularly true in linear regression models, where the regression coefficients indicate how much each variable influences the outcome, if the data is standardized. Let's look at a standardized version of this table:

| Catalyst    | Std. Oxidation Potential | Std. Oxidation State  | Std. Absorption Wavelength |  Std. Yield  |
|-------------|--------------------------|-----------------------|----------------------------|--------------|
|    A        |       0.21               |        1.0            |          0.9               |       -0.1   |      
|    B        |       1.5                |       -1.0            |         -1.2               |        1.3   |      
|    C        |      -1.2                |       -1.0            |         -0.8               |        0.3   |      
|    D        |      -0.48               |        1.0            |          1.1               |       -1.5   |

You will notice that the entries are now unitless. This is because we divide each datapoint by the standard deviation of the corresponding variable, which has the same unit. This means that our representation is now invariant to the units of the input data. If the wavelength were measured in $\mu m$, it would still give us the same standardized data. Another advantage of standardization is that it immediately shows us if a given variable is above or below average. For example, Catalysts B and C have above average yield and below average Absorption Wavelengths and Oxidation states, while the reverse is true for A and D. This is a preliminary indication that there might be some relationships in the data, which is easier to see if the data is standardized.  

## Categorical and discrete data

Some measurements do not yield numerical data, however. Certain nanoparticles can be grown as cubes, spheres or rods, depending on the reaction conditions. Or I may want to differenciate catalysts that contain noble metals from ones that contain only earth-abundant ones. These are examples of *categorical* data. In the simplest case of two different categories, this type of information is also called *binary* and can simply be encoded as 0 or 1 (e.g. 1 if a noble metal is used, 0 if not). In principle, this same idea can be extended to multiple categories, i.e. using 0 for cubic, 1 for spherical and 2 for rod-shaped nanoparticles. However, in this case we are introducing some bias into the representation, because this implies that cubes are more similar to spheres than to rods (because 0 is closer to 1 than to 2). We can argue about whether this is the case or not. The point is, an integer encoding of categories implies some ranking or relationship among the categories. In order to avoid this kind of bias we can use a so-called *one-hot encoding*.

### One-Hot Encoding

The idea behind one-hot encoding is to replace a collumn of categorical data with a matrix of numerical (binary) data. Consider the following data-table, which shows the shapes of nanoparticles obtained with different synthesis recipes:

| Reaction Route      | Shape        | 
|---------------------|--------------|
|    A                |  cube        |      
|    B                |  rod         |      
|    C                |  sphere      |      
|    D                |  cube        |
|    E                |  sphere      |
|    F                |  sphere      |

One-hot encoding of the *Shape* collumn leads to:

| Reaction Route      | Cube         | Sphere   | Rod    | 
|---------------------|--------------|----------|--------|
|    A                |  1           |    0     |   0    |      
|    B                |  0           |    0     |   1    |      
|    C                |  0           |    1     |   0    |  
|    D                |  1           |    0     |   0    |
|    E                |  0           |    1     |   0    |
|    F                |  0           |    1     |   0    |

We thus obtain a new collumn for each category label. Since the categories are mutually exclusive, for each line there is only a single 1 and all other collumns are marked as 0. With this we have achieved our goal of removing any implied relationships between the categories. Now each recipe has a corresponding one-hot vector. For example, recipe A is represented by the vector $(1,0,0)$. 

### Representing Chemical Elements

The chemical elements are an interesting special case. Are they numerical or categorical? We could simply use the atomic number $Z$ as a numerical representation. However, this leads to the problem we just discussed: This representation implies that atoms with similar atomic numbers also have similar properties. That is not really true: In many ways, Carbon ($Z$=6) is more similar to Silicon ($Z$=12) or Germanium ($Z$=32) than to Nitrogen ($Z$=7). To remove this bias from the representation, we can use a one-hot encoding instead. This way we remove all notion of similarity between elements from the representation and treats the element type as categorical data.

However, this arguably takes things too far. There *are* similarities between elements, but the atomic number is not a good way to encode them. There are some interesting alternatives. The Pettifor number, for example, sorts the elements such that elements that are found in similar bonding patterns are numerically close to each other. Another idea is to use the period and group in the periodic table as a two-dimensional representation. These are again examples of including prior information into the representation, which is usually a good idea. 

However, there is no universal measure of similarity between elements, it depends on the property of interest. Fortunately, numerical representations of categorical data can also be *learned* from data via so called *embeddings*. How this works in detail will be introduced in the chapter on neural networks. Below you can see such an embedding from a model trained to predict formation energies of elpasolite crystals.

```{figure} notebooks/embeddings.png
:alt: "Element embeddings"
:width: 400px
:align: center

Element embeddings learned by a model predicting formation energies of elpasolite crystals.
```

In this plot, the distance between two elements translates to their similarity, as judged by the ML model. We find some distinct clusters that match groups in the periodic table (e.g. He, Ne, Xe, Ar or Na, K, Cs, Rb). But there are also some features that are rather unexpected, e.g. that Kr is far from the other noble gases or that Br and F are far from Cl and I. This is because such a data-driven embedding is not a general representation of elemental similarity, but tailored to the specific chemistry of elpasolite crystals. It seems that in this specific case, Br and F are more similar to each other than F and Cl.

## Chemical structures

Arguably, the strucures of molecules and solids are the richest form of chemical data available. Physicochemical data is usually not very high dimensional, and even when it is (e.g. in time dependent spectroscopy) there is often a sophisticated physical model that can be used to analyze and fit the data. In this case, there is no need for ML methods and more traditional statistical analysis should be preferred. Structural data, on the other hand, is almost always high-dimensional. Furthermore, there are in general no simple predictive models for so-called *structure-property relationships*. Consequently, the vast majority of chemical ML deals with structures as input and/or output. In the following, we will therefore introduce different approaches of representing chemical structures. 

### The molecular graph

The most widely used representations of molecules in everyday live are Lewis structures and skeletal formulas, which you find in text books and ChemDraw files, on fumehood windows and coffee mugs.

```{figure} notebooks/Skeletal_Lewis_examples.png
:alt: "Skeletal and Lewis structures"
:width: 600px
:align: center

Skeletal and Lewis structure drawings of organic molecules. Skeletal formulas omit certain details like hydrogen atoms and labels for carbon atoms. These are implicit in the valence rules of organic chemistry.
```

This type of two dimensional picture basically shows the chemical elements that the molecule contains and how they are connected to each other by chemical bonds. Lots of additional information may be contained: bond-orders, charges, lone electron pairs and even 3D information like chiral descriptors. Depending on the application some information may also be left out, to increase the clarity. In the case of skeletal formulas, Hydrogen atoms are ommited for example.

As we have extensively discussed above, ML methods need numerical inputs so these pictures are not ideal. In principle, we could borrow the methods of image recognition, but this is not very efficient or transferable. A more straightforward way to make the information in a Lewis structure machine readable is by using a mathematical object called a *graph*. This should not be confused with the more commonly encountered use of the term *graph* for a visualization of data. The graphs we are talking about define network-like objects that consist of so-called *nodes* that are connected to each other via *edges* .

```{figure} notebooks/Molecular_Graph_Example.png
:alt: "Example Molecular Graph"
:width: 600px
:align: center

Molecular graph of methanol (left). The graph only encodes the connections (edges) between atoms (nodes), not their full geometry. Mathematically, it can be encoded as an $N \times N$ adjacency matrix (right). In addition, properties of the atoms (e.g. their elements) can be encoded as $N$-dimensional vectors of node properties. 
```

Graphs are a natural way to represent certain types of data, such as social networks (nodes are people, edges are relationships), power grids (nodes are power plants and consumers, edges are power lines) and, of course, molecules. In this case the nodes represent atoms and the edges may represent bonds (or other interactions). ML models usually process graphs via their adjacency matrix. For a molecule with $N$ atoms, this is an $N \times N$ matrix, usually abbreviated as $\mathbf{A}$. In the simplest case, the elements $A_{ij}$ of this matrix are 1 if there is a bond between $i$ and $j$ and 0 otherwise, but we can in principle also encode the bond order (or other *edge features*) in $\mathbf{A}$. Additionally, the nodes usually also have some properties (e.g. the atomic number or a more suitable representation of the element).


### String representations

As an alternative to graphs, *string* representations are also widely used in chemical ML. A string is simply a sequence of characters, like a word or a sentence. The most famous of these is the SMILES representation, which stands for *Simplified Molecular Input Line Entry System*. SMILES was not originally developed for ML, but more generally as a chemical notation for computers. 

One advantage of string representations is that they are more easily readable for humans. For example, the string `O=CC` represents the structure of acetaldehyde. Even if you have never heard of SMILES, you can probably figure out how this works: The letters stand for atoms, and `=` represents a double bond. SMILES is a very compact notation so that many details are implicit. For example, no bond symbol is used between the `C` atoms, so by default we assume that they are connected by a single bond. Furthermore, as with skeletal formulas, hydrogens are generally not explicitly represented. We can infer these implicit features from normal valence rules. A more explicit representation of acetaldehyde would be `[O]=[CH]-[CH3]`. This is also a valid SMILES string, but the implicit version is much more common.

Here are some more examples of molecular structures and the corresponding SMILES strings:


```{figure} notebooks/SMILES_examples.png
:alt: "SMILES and corresponding structures"
:width: 800px
:align: center

Organic molecules and the corresponding SMILES strings.
```

If you're unfamiliar with SMILES, not all of these string will be obviously at first. Since chemical structures can become very complicated, the SMILES language is also quite complicated. A detailed introduction would therefore be beyond the scope of this course. The most important symbols are shown below:

| Symbol     | Meaning                  | Example    |
|------------|--------------------------|------------|
|   -        |  Single Bond             |  C-O       |
|   =        |  Double Bond             |  C=C       |
|   #        |  Triple Bond             |  N#C       |
|   ()       |  Branch                  |  CC(O)C    |
|   1...1    |  Rings                   |  C1CCCCC1  |

There are several variations and extensions of SMILES, e.g. to represent chemical reactions or patterns and substructures (SMARTS). Recently a new string representation called SELFIES has been proposed, which is particularly useful in the context of generative ML models. String representations are themselves of course not numerical and thus difficult to use as raw inputs in ML models. In practice it is therefore common to use the encodings we discussed for categorical data above, i.e. one-hot encodings or embeddings.

In principle, string representations like SMILES and molecular graphs encode the same information: What are the atoms and how are they connected? Mathematically speaking, there is a bijection between them. This means that we can transform strings to graphs and back without loosing information. So why do we need these different types of representations at all? This has mainly practical reasons. If we work with graphs, we can use all the ML technology that was developed for graph data. If we work with strings, we can use the technology of natural language processing (NLP). To some extent this is a matter of taste, but it also depends on the task of interest. For example, graphs are widely used for regression and classification models while strings work very well in molecular design or reaction planning tasks. 

## 3D representations

Lewis structures contain a lot of information about molecules, but they are still abstractions. Real molecules are three dimensional, dynamic objects. In molecular modeling it is common to represent this information in structure files, e.g. using Cartesian coordinates. In this representation, methane can be expressed as:

```
5

C      0.00000    0.00000    0.00000
H      0.00000    0.00000    1.08900
H      1.02672    0.00000   -0.36300
H     -0.51336   -0.88916   -0.36300
H     -0.51336    0.88916   -0.36300
```

This contains the full 3D information about the molecule: The number of atoms, the chemical element of each atom and its Cartesian (xyz) coordinates (in Angstrom). Nevertheless, raw coordinates are almost never used as an ML representation. This is because they are very inconvenient from the perspective of *invariances*. If we *rotate* or *translate* (move) a molecule, all its coordinates change. But many important molecular properties (e.g. the total energy) are invariant to these transformations. The same is true for *permutations* (i.e. switching two atoms). Consequently, 3D representations for ML are ideally invariant to rotations, translations and permutations.


```{figure} notebooks/invariances.png
:alt: "Invariances of chemical representations"
:width: 600px
:align: center

Representations of 3D structures should ideally be invariant to translations, rotations and permutations. This is because many important molecular properties (energies, ionization potentials, excitation energies, etc.) are also invariant to such changes.
```

Rotations and translations can fairly easily be removed by using internal coordinates such as interatomic distances instead of the cartesian coordinates. The matter of permutations is trickier, however. We will discuss how to deal with this in two different approaches, namely global and local representations.

### Global representations: Coulomb Matrix and Many Body Tensor Representation

The goal of a *global* representation is to encode the structure of an entire molecule or material in a single mathematical object, typically a vector or a matrix. The adjacency matrix of a molecular graph would thus be an example of a global representation. A closely related representation is the Coulomb Matrix. This is also an $N \times N$ matrix ($\mathbf{M}^{\mathrm{Coulomb}}$), but instead of encoding bonds between atoms, the off-diagonal elements contain the Coulomb repulsion between the corresponding nuclei, while the diagonal elements also depend on the atomic number of the element:

$$
M_{AB}^{\mathrm{Coulomb}} =
\begin{cases}
0.5 Z_{A}^{2.4} & \text{for} & A=B\\
\frac{Z_{A}Z_{B}}{r_{AB}} & \text{for} & A \neq B
\end{cases}
$$

The Coulomb matrix was developed in one of the seminal papers on chemical ML, but it is rarely used today for several reasons. Firstly, it is not permutationally invariant. If we exchange the order of the atoms we get a different matrix. Secondly, the size of the matrix depends on the number of atoms in the system. This is a problem because ML models like neural networks usually expect the inputs to be of a fixed size. 

These issues can be mitigated to some extent. For example, permutational invariance can be achieved by choosing a canonical ordering of the rows and collumns. For variable input sizes, we can also pad the matrix with zeros. Such fixes often lead to new problems down the line, however. The form of the diagonal and off-diagonal elements also shows some feature engineering, which is not necessarily benefitial in this case. Is the interaction between two atoms really meaningfully represented by the Coulomb interaction between their nuclei?

Due to these shortcomings, newer representations take different approaches to ensure permutational invariance and a fixed size of the representation (independent of the number of atoms). One of the most common ways is to use histograms or distribution functions of internal coordinates. An example of this is the many-body tensor representation (MBTR). 

The MBTR is based on the many-body expansion of the total energy, which reads:

$$
E = \sum_{A} e_{A} + \sum_{A<B} e_{AB} + \sum_{A<B<C} e_{ABC} + \sum_{A<B<C<D} e_{ABCD} ...
$$

This expresses the total energy of a system as a sum of one-body, two-body, three-body, etc. energy contributions $e$. In a classical forcefield, these contributions are usually described by bond, angular and dihedral functions, so the idea of the MBTR is to build a representation using these ingredients as features. Note that, the many body expansion in principle has as many terms as there are atoms in the system, in which case it is an exact (and very complicated) way to express the energy. The advantage is that higher order terms generally contribute fairly little to the total energy, so that in practice we can truncate the series to a good approximation.

To construct a finite size representation from these ingredients, the MBTR next builds smooth distribution functions of these features, as shown below for water:

```{figure} notebooks/MBTR_composite.png
:alt: "Water geometry"
:width: 800px
:align: center

The Many-Body-Tensor Representation of a single Water molecule.  
```

The big advantage of these distribution functions is that they are naturally invariant to permutations between atoms. For the two-body part of the MBTR representation, we get a separate distribution function for each element pair (C-C, C-H, C-O, O-O, O-H, H-H). Each of these can then be transformed into a fixed-size vector by discretizing the function on a uniform grid. We thus obtain $N_{\mathrm{el}}(N_{\mathrm{el}}+1)/2$ two-body vectors for a system containing $N_\mathrm{el}$ elements (i.e. the number if unique elements in a symmetric matrix). This is the origin behind the term *tensor* in MBTR. Loosely speaking, tensors are simply multi-dimensional arrays and we will learn a lot more about them when we talk about neural networks. For now, it is enough to understand that the size of the many-body tensor depends on the number of elements in the system and the order of the many body expansion. Because high-order terms lead to very large tensors and are generally less important, the MBTR is usually limited to two- or three-body terms. Importantly, however, the size of the representation does *not* depend on the number of atoms in the system.

There are a lot of details which I skipped in this description. For example, there are different ways of encoding the interatomic distance $r_{AB}$ (a common choice is $\frac{1}{r_{AB}}$) or the angles $\alpha_{ABC}$ (e.g. as $\cos(\alpha_{ABC})$). Furthermore, it is important to use distant dependent damping functions for higher-order terms, because angles between distant atoms are much less relevant than the ones between directly bonded atoms.

 
### Local representations: Smooth Overlap of Atomic Positions

A completely different approach to achieve a permutationally invariant and finite-size representation is to use a *local* representation. In this context, local basically means *per atom*. This is obviously a good idea for atomic properties like partial charges or NMR chemical shifts. However, the local approach is also very commonly used for *global* molecular properties like the total energy.

This is based on the idea that many molecular properties (most importantly the energy) can approximately be decomposed into atomic contributions $\epsilon_{A}$:

$$
E \approx \sum_{A} \epsilon_{A}(\chi_A)
$$

At first glance, this looks like a many-body expansion truncated at the one-body terms, which would be rather useless. The key difference to the many-body expansion, however, is that we introduced the variable $\chi_A$, which represents the neighborhood (or environment) of atom $A$. The motivation behind this approximation is that many chemical properties are mostly local. This is why functional groups are such a useful abstraction: The properties of a carbonyl group are somewhat influenced by the atoms to which it is connected, but distant atoms only have a very small influence. $\chi_A$ can be understood as a continuous generalization of the functional group idea. The goal of local representations is to find a useful numerical expression for $\chi_A$, usually in the form of a vector.

The prototypical local representation which we will discuss is the Smooth Overlap of Atomic Positions (SOAP). The key concept in this context are the so-called *atomic density functions* $\rho_{Z}(r)$, which quantify the likelihood of finding an atom of type $Z$ at position $r$. For a single geometry, this function is $1$ at the location of an atom and $0$ otherwise (or, to be more precise: it consists of a $\delta$-function at each atomic position). For ML applications, it is benefitial to use a smoothened version of the atomic density, so that there is some overlap between similar but not identical geometries. In SOAP this is achieved by placing a Gaussian function on top of each atom, which yields a function that looks similar to the electron density. 

To obtain a local representation from the atomic density function, we next introduce a sphere with a fixed cutoff radius $r_\mathrm{cut}$ around each atom. The SOAP representation of each atom will only describe the neighborhood density within that cutoff. To avoid discontinuities when neighboring atoms move in and out of this sphere, we additionally define a damping function which smoothly goes to zero at the cutoff. This gives us *neighborhood density functions* $\rho_{Z,A}(\mathbf{r})$, which describe the distribution of atoms of type $Z$ around the central atom $A$. These neighborhood density functions thus encode the local chemical environment around a given atom, which is what $\chi_A$ is supposed to represent. 

```{figure} notebooks/Atomic_Environment_Composition.png
:alt: "Visualization of Atomic Environments"
:width: 400px
:align: center

The local atomic environment of each atom is defined by a sphere with a given cutoff radius, here illustrated for the central carbon atom. This is translated into a 3D density function by broadening each atom with a Gaussian, as illustrated on the right (the central atom is marked by a cross). 
```

Unfortunately, we cannot use the neighborhood density functions (e.g. on a uniform grid) as a representation, since they are not rotationally invariant. Returning to the functional group example: A carbonyl group is always a carbonyl group, no matter how it is oriented in space. To achieve a compact and rotationally invariant representation, SOAP introduces one final component, namely a set of orthogonal basis functions ($b_{nlm}$) centered on $A$. These are defined in spherical coordinates as:

$$
b_{nlm}(r,\theta,\phi) = g_{n}(r)Y_{lm}(\theta,\phi)
$$

These functions are defined by a radial basis function $g_{n}(r)$ and a spherical harmonic function $Y_{lm}(\theta,\phi)$. You may be familiar with this type of basis, as it is very similar to the basis sets used in many quantum chemistry codes. The radial and angular basis functions are hierarchical and defined via the indices $n$ and $l$, respectively (for a given $l$, a *shell* of $2l+1$ spherical harmonics is defined with $-l \leq m \geq l$). We can thus tune the radial and angular resolution of the basis set by using more of the corresponding basis functions. The number of functions is usually specified by the largest index used, i.e. all radial functions up to $n_\mathrm{max}$ and angular functions up to $l_\mathrm{max}$ are used. These functions are illustrated below:

```{figure} notebooks/SOAP_basis_functions_scale.png
:alt: "SOAP basis functions"
:width: 800px
:align: center

SOAP basis functions in 2D. The index $n$ enumerates radial basis functions, $l$ and $m$ enumerate the angular basis functions. Note that in 2D the angular basis functions are simply sine and cosine functions, in 3D they are spherical harmonics. Consequently, the values of $m$ range from $-l$ to $l$ in 3D but are only zero or one (independent of $l$) in 2D.
```
Since the basis functions $b_{nlm}$ are orthogonal, we can obtain a representation of the neighborhood density in this basis by projection. This means that we can express $\rho_{Z,A}(r)$ as a linear combination of the functions $b_{nlm}$:

$$
\rho_{Z,A}(\mathbf{r}) \approx \sum_{n=0}^{n_\mathrm{max}} \sum_{l=0}^{l_\mathrm{max}} \sum_{m=-l}^{l} c_{nlm} b_{nlm}(\mathbf{r})
$$
with

$$
c_{nlm} = \iiint b_{nlm}(\mathbf{r}) \rho_{Z,A}(\mathbf{r}) d\mathbf{r}
$$

The information about the neighborhood density is now collected in the coefficients $c_{nlm}$, which is more compact than dealing with the full function $\rho_{Z,A}(\mathbf{r})$ on a grid. However, this is still not rotationally invariant because of the angular information contained in the spherical harmonics: 

```{figure} notebooks/SOAP_rotate_composite.png
:alt: "SOAP_rotate"
:width: 800px
:align: center

Two rotated chemical environments and the corresponding weighted SOAP basis functions. At first glance, the weighted basis functions look very similar. Upon closer inspection, you will notice that the sign of the functions with $l>0$ is flipped. More generally, rotations lead to a redistribution of the coefficients within a given angular momentum channel.
```
As you can notice in the above figure, rotating the chemical environment leads to different weights for the basis functions. Fortunately, this is not random. In fact, we can derive a series of invariant quantities from the $c_{nlm}$ coefficients, the simplest being the power-spectrum $p_{nn'l}$:

$$
p_{nn'l} = \pi \sqrt{\frac{8}{2l+1}} \sum_{m=-l}^{l} c_{nml}c_{n'ml}
$$

This means that, for a given angular momentum $l$ and two radial functions $n$ and $n'$, we sum over products of coefficients for all possible $m$ indices. By summing over the $m$s, the rotational information is removed so that the $p_{nn'l}$ are rotationally invariant features representing a chemical environment. We usually collect these in a single, normalized vector $\mathbf{p}$.

Let's briefly recapitulate: We started from the idea that the structure of a molecule can be encoded in atomic density functions $\rho_{Z}$. Then we decided to focus on the atomic neighborhood $\rho_{Z,A}$ of each atom, using a smooth spherical cutoff function. This neighborhood was the expanded in a basis set centered on the atom of interest, yielding a set of coefficients $c_{nml}$. Finally, these coefficients were transformed into the *rotationally invariant* powerspectrum $\mathbf{p}$. Note that permutational invariance is also taken are of, since the neighborhood density functions are automatically invariant to permutations.

Despite all these nice formal properties, it might be hard to understand how such a representation can be used in practice. To illustrate this, I will foreshadow a little bit on the chapter on Kernel ML. In ML a kernel function is a function that measures the *similarity* between two representations. There are many such kernels, but the simplest one is the *linear Kernel*, which is defined as the dot product between two representations:

$$
K(\mathbf{p}_{A},\mathbf{p}_{B}) = \mathbf{p}_{A}^T \mathbf{p}_{B}
$$

If the powerspectrum vectors are normalized, $K(\mathbf{p}_{A},\mathbf{p}_{B})$ will range from 1 (for identical vectors) to 0 (for orthogonal vectors). Thus, the closer $K(\mathbf{p}_{A},\mathbf{p}_{B})$ is to one, the higher the similarity between the atomic environments. This is best understood by looking at an example:

```{figure} notebooks/SOAP_similarity_composition.png
:alt: "SOAP_similarity"
:width: 800px
:align: center

Dot product similarity between 2D SOAP powerspectra corresponding to the environments depitcted in the top row. The central atom is surrounded by two neighbors, one of which is rotated by an angle $\phi$ between two perpendicular configurations. The similarity is measured with respect to the leftmost configuration and consequently starts at a value of one. As the angle increases, the similarity decreases and reaches a minimum for the linear configuration. After this, the similarity begins to rise again, reaching one for the other perpendicular configuration.
```

One can see from this figure that the two perpendicular environments are considered to be identical ($K(\mathbf{p}_{A},\mathbf{p}_{B}=1$) and that the linear configuration is most dissimilar to these. This makes intuitive sense. Furthermore, the similarity is a smooth function: As the angle increases, the similarity decreases gradually until the linear configuration is reached. Again, this makes sense intuitively. Because chemical properties like the energy usually also change smoothly when chemical environments change (e.g., when a chemical bond is formed or broken), representations like SOAP are therefore extremely useful.
 
%## Chemical databases

%QM, Zinc etc  
%Big data vs small data

## Key concepts

